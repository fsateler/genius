/* A Bison parser, made by GNU Bison 3.7.  */

/* Bison implementation for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015, 2018-2020 Free Software Foundation,
   Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* DO NOT RELY ON FEATURES THAT ARE NOT DOCUMENTED in the manual,
   especially those whose name start with YY_ or yy_.  They are
   private implementation details that can be changed or removed.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output.  */
#define YYBISON 1

/* Bison version.  */
#define YYBISON_VERSION "3.7"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 0

/* Push parsers.  */
#define YYPUSH 0

/* Pull parsers.  */
#define YYPULL 1




/* First part of user prologue.  */
#line 21 "parse.y"

#include "config.h"

#include <glib.h>
#include <string.h>
#include "structs.h"
#include "mpwrap.h"
#include "eval.h"
#include "dict.h"
#include "util.h"
#include "calc.h"
#include "matrix.h"
#include "matrixw.h"
	
#include "parseutil.h"

extern GSList *gel_parsestack;

extern gboolean gel_return_ret; /*should the lexer return on \n*/

/* prototype for yylex */
int yylex(void);
void yyerror(const char *);


#line 97 "y.tab.c"

# ifndef YY_CAST
#  ifdef __cplusplus
#   define YY_CAST(Type, Val) static_cast<Type> (Val)
#   define YY_REINTERPRET_CAST(Type, Val) reinterpret_cast<Type> (Val)
#  else
#   define YY_CAST(Type, Val) ((Type) (Val))
#   define YY_REINTERPRET_CAST(Type, Val) ((Type) (Val))
#  endif
# endif
# ifndef YY_NULLPTR
#  if defined __cplusplus
#   if 201103L <= __cplusplus
#    define YY_NULLPTR nullptr
#   else
#    define YY_NULLPTR 0
#   endif
#  else
#   define YY_NULLPTR ((void*)0)
#  endif
# endif

/* Use api.header.include to #include this header
   instead of duplicating it here.  */
#ifndef YY_YY_Y_TAB_H_INCLUDED
# define YY_YY_Y_TAB_H_INCLUDED
/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 1
#endif
#if YYDEBUG
extern int yydebug;
#endif

/* Token kinds.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    YYEMPTY = -2,
    YYEOF = 0,                     /* "end of file"  */
    YYerror = 256,                 /* error  */
    YYUNDEF = 257,                 /* "invalid token"  */
    STARTTOK = 258,                /* STARTTOK  */
    LOADFILE = 259,                /* LOADFILE  */
    LOADFILE_GLOB = 260,           /* LOADFILE_GLOB  */
    LOAD_PLUGIN = 261,             /* LOAD_PLUGIN  */
    CHANGEDIR = 262,               /* CHANGEDIR  */
    PWD = 263,                     /* PWD  */
    LS = 264,                      /* LS  */
    LS_ARG = 265,                  /* LS_ARG  */
    HELP = 266,                    /* HELP  */
    HELP_ARG = 267,                /* HELP_ARG  */
    NUMBER = 268,                  /* NUMBER  */
    STRING = 269,                  /* STRING  */
    FUNCID = 270,                  /* FUNCID  */
    FUNCTION = 271,                /* FUNCTION  */
    CALL = 272,                    /* CALL  */
    THREEDOTS = 273,               /* THREEDOTS  */
    PARAMETER = 274,               /* PARAMETER  */
    RETURNTOK = 275,               /* RETURNTOK  */
    BAILOUT = 276,                 /* BAILOUT  */
    EXCEPTION = 277,               /* EXCEPTION  */
    CONTINUE = 278,                /* CONTINUE  */
    BREAK = 279,                   /* BREAK  */
    LOCAL = 280,                   /* LOCAL  */
    WHILE = 281,                   /* WHILE  */
    UNTIL = 282,                   /* UNTIL  */
    FOR = 283,                     /* FOR  */
    SUM = 284,                     /* SUM  */
    PROD = 285,                    /* PROD  */
    DO = 286,                      /* DO  */
    IF = 287,                      /* IF  */
    THEN = 288,                    /* THEN  */
    ELSE = 289,                    /* ELSE  */
    TO = 290,                      /* TO  */
    BY = 291,                      /* BY  */
    IN = 292,                      /* IN  */
    AT = 293,                      /* AT  */
    MAKEIMAGPARENTH = 294,         /* MAKEIMAGPARENTH  */
    SEPAR = 295,                   /* SEPAR  */
    NEXTROW = 296,                 /* NEXTROW  */
    EQUALS = 297,                  /* EQUALS  */
    DEFEQUALS = 298,               /* DEFEQUALS  */
    SWAPWITH = 299,                /* SWAPWITH  */
    TRANSPOSE = 300,               /* TRANSPOSE  */
    ELTELTDIV = 301,               /* ELTELTDIV  */
    ELTELTMUL = 302,               /* ELTELTMUL  */
    ELTELTPLUS = 303,              /* ELTELTPLUS  */
    ELTELTMINUS = 304,             /* ELTELTMINUS  */
    ELTELTEXP = 305,               /* ELTELTEXP  */
    ELTELTMOD = 306,               /* ELTELTMOD  */
    DOUBLEFACT = 307,              /* DOUBLEFACT  */
    EQ_CMP = 308,                  /* EQ_CMP  */
    NE_CMP = 309,                  /* NE_CMP  */
    CMP_CMP = 310,                 /* CMP_CMP  */
    LT_CMP = 311,                  /* LT_CMP  */
    GT_CMP = 312,                  /* GT_CMP  */
    LE_CMP = 313,                  /* LE_CMP  */
    GE_CMP = 314,                  /* GE_CMP  */
    LOGICAL_XOR = 315,             /* LOGICAL_XOR  */
    LOGICAL_OR = 316,              /* LOGICAL_OR  */
    LOGICAL_AND = 317,             /* LOGICAL_AND  */
    LOGICAL_NOT = 318,             /* LOGICAL_NOT  */
    INCREMENT = 319,               /* INCREMENT  */
    LOWER_THAN_ELSE = 320,         /* LOWER_THAN_ELSE  */
    LOWER_THAN_INCREMENT = 321,    /* LOWER_THAN_INCREMENT  */
    MOD = 322,                     /* MOD  */
    ELTELTBACKDIV = 323,           /* ELTELTBACKDIV  */
    UMINUS = 324,                  /* UMINUS  */
    UPLUS = 325                    /* UPLUS  */
  };
  typedef enum yytokentype yytoken_kind_t;
#endif
/* Token kinds.  */
#define YYEOF 0
#define YYerror 256
#define YYUNDEF 257
#define STARTTOK 258
#define LOADFILE 259
#define LOADFILE_GLOB 260
#define LOAD_PLUGIN 261
#define CHANGEDIR 262
#define PWD 263
#define LS 264
#define LS_ARG 265
#define HELP 266
#define HELP_ARG 267
#define NUMBER 268
#define STRING 269
#define FUNCID 270
#define FUNCTION 271
#define CALL 272
#define THREEDOTS 273
#define PARAMETER 274
#define RETURNTOK 275
#define BAILOUT 276
#define EXCEPTION 277
#define CONTINUE 278
#define BREAK 279
#define LOCAL 280
#define WHILE 281
#define UNTIL 282
#define FOR 283
#define SUM 284
#define PROD 285
#define DO 286
#define IF 287
#define THEN 288
#define ELSE 289
#define TO 290
#define BY 291
#define IN 292
#define AT 293
#define MAKEIMAGPARENTH 294
#define SEPAR 295
#define NEXTROW 296
#define EQUALS 297
#define DEFEQUALS 298
#define SWAPWITH 299
#define TRANSPOSE 300
#define ELTELTDIV 301
#define ELTELTMUL 302
#define ELTELTPLUS 303
#define ELTELTMINUS 304
#define ELTELTEXP 305
#define ELTELTMOD 306
#define DOUBLEFACT 307
#define EQ_CMP 308
#define NE_CMP 309
#define CMP_CMP 310
#define LT_CMP 311
#define GT_CMP 312
#define LE_CMP 313
#define GE_CMP 314
#define LOGICAL_XOR 315
#define LOGICAL_OR 316
#define LOGICAL_AND 317
#define LOGICAL_NOT 318
#define INCREMENT 319
#define LOWER_THAN_ELSE 320
#define LOWER_THAN_INCREMENT 321
#define MOD 322
#define ELTELTBACKDIV 323
#define UMINUS 324
#define UPLUS 325

/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
union YYSTYPE
{
#line 47 "parse.y"

	mpw_t val;
	char *id;

#line 294 "y.tab.c"

};
typedef union YYSTYPE YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif


extern YYSTYPE yylval;

int yyparse (void);

#endif /* !YY_YY_Y_TAB_H_INCLUDED  */
/* Symbol kind.  */
enum yysymbol_kind_t
{
  YYSYMBOL_YYEMPTY = -2,
  YYSYMBOL_YYEOF = 0,                      /* "end of file"  */
  YYSYMBOL_YYerror = 1,                    /* error  */
  YYSYMBOL_YYUNDEF = 2,                    /* "invalid token"  */
  YYSYMBOL_STARTTOK = 3,                   /* STARTTOK  */
  YYSYMBOL_LOADFILE = 4,                   /* LOADFILE  */
  YYSYMBOL_LOADFILE_GLOB = 5,              /* LOADFILE_GLOB  */
  YYSYMBOL_LOAD_PLUGIN = 6,                /* LOAD_PLUGIN  */
  YYSYMBOL_CHANGEDIR = 7,                  /* CHANGEDIR  */
  YYSYMBOL_PWD = 8,                        /* PWD  */
  YYSYMBOL_LS = 9,                         /* LS  */
  YYSYMBOL_LS_ARG = 10,                    /* LS_ARG  */
  YYSYMBOL_HELP = 11,                      /* HELP  */
  YYSYMBOL_HELP_ARG = 12,                  /* HELP_ARG  */
  YYSYMBOL_NUMBER = 13,                    /* NUMBER  */
  YYSYMBOL_STRING = 14,                    /* STRING  */
  YYSYMBOL_FUNCID = 15,                    /* FUNCID  */
  YYSYMBOL_FUNCTION = 16,                  /* FUNCTION  */
  YYSYMBOL_CALL = 17,                      /* CALL  */
  YYSYMBOL_THREEDOTS = 18,                 /* THREEDOTS  */
  YYSYMBOL_PARAMETER = 19,                 /* PARAMETER  */
  YYSYMBOL_RETURNTOK = 20,                 /* RETURNTOK  */
  YYSYMBOL_BAILOUT = 21,                   /* BAILOUT  */
  YYSYMBOL_EXCEPTION = 22,                 /* EXCEPTION  */
  YYSYMBOL_CONTINUE = 23,                  /* CONTINUE  */
  YYSYMBOL_BREAK = 24,                     /* BREAK  */
  YYSYMBOL_LOCAL = 25,                     /* LOCAL  */
  YYSYMBOL_WHILE = 26,                     /* WHILE  */
  YYSYMBOL_UNTIL = 27,                     /* UNTIL  */
  YYSYMBOL_FOR = 28,                       /* FOR  */
  YYSYMBOL_SUM = 29,                       /* SUM  */
  YYSYMBOL_PROD = 30,                      /* PROD  */
  YYSYMBOL_DO = 31,                        /* DO  */
  YYSYMBOL_IF = 32,                        /* IF  */
  YYSYMBOL_THEN = 33,                      /* THEN  */
  YYSYMBOL_ELSE = 34,                      /* ELSE  */
  YYSYMBOL_TO = 35,                        /* TO  */
  YYSYMBOL_BY = 36,                        /* BY  */
  YYSYMBOL_IN = 37,                        /* IN  */
  YYSYMBOL_AT = 38,                        /* AT  */
  YYSYMBOL_MAKEIMAGPARENTH = 39,           /* MAKEIMAGPARENTH  */
  YYSYMBOL_SEPAR = 40,                     /* SEPAR  */
  YYSYMBOL_NEXTROW = 41,                   /* NEXTROW  */
  YYSYMBOL_EQUALS = 42,                    /* EQUALS  */
  YYSYMBOL_DEFEQUALS = 43,                 /* DEFEQUALS  */
  YYSYMBOL_SWAPWITH = 44,                  /* SWAPWITH  */
  YYSYMBOL_TRANSPOSE = 45,                 /* TRANSPOSE  */
  YYSYMBOL_ELTELTDIV = 46,                 /* ELTELTDIV  */
  YYSYMBOL_ELTELTMUL = 47,                 /* ELTELTMUL  */
  YYSYMBOL_ELTELTPLUS = 48,                /* ELTELTPLUS  */
  YYSYMBOL_ELTELTMINUS = 49,               /* ELTELTMINUS  */
  YYSYMBOL_ELTELTEXP = 50,                 /* ELTELTEXP  */
  YYSYMBOL_ELTELTMOD = 51,                 /* ELTELTMOD  */
  YYSYMBOL_DOUBLEFACT = 52,                /* DOUBLEFACT  */
  YYSYMBOL_EQ_CMP = 53,                    /* EQ_CMP  */
  YYSYMBOL_NE_CMP = 54,                    /* NE_CMP  */
  YYSYMBOL_CMP_CMP = 55,                   /* CMP_CMP  */
  YYSYMBOL_LT_CMP = 56,                    /* LT_CMP  */
  YYSYMBOL_GT_CMP = 57,                    /* GT_CMP  */
  YYSYMBOL_LE_CMP = 58,                    /* LE_CMP  */
  YYSYMBOL_GE_CMP = 59,                    /* GE_CMP  */
  YYSYMBOL_LOGICAL_XOR = 60,               /* LOGICAL_XOR  */
  YYSYMBOL_LOGICAL_OR = 61,                /* LOGICAL_OR  */
  YYSYMBOL_LOGICAL_AND = 62,               /* LOGICAL_AND  */
  YYSYMBOL_LOGICAL_NOT = 63,               /* LOGICAL_NOT  */
  YYSYMBOL_INCREMENT = 64,                 /* INCREMENT  */
  YYSYMBOL_LOWER_THAN_ELSE = 65,           /* LOWER_THAN_ELSE  */
  YYSYMBOL_LOWER_THAN_INCREMENT = 66,      /* LOWER_THAN_INCREMENT  */
  YYSYMBOL_MOD = 67,                       /* MOD  */
  YYSYMBOL_68_ = 68,                       /* ':'  */
  YYSYMBOL_69_ = 69,                       /* '+'  */
  YYSYMBOL_70_ = 70,                       /* '-'  */
  YYSYMBOL_71_ = 71,                       /* '*'  */
  YYSYMBOL_72_ = 72,                       /* '/'  */
  YYSYMBOL_73_ = 73,                       /* '\\'  */
  YYSYMBOL_ELTELTBACKDIV = 74,             /* ELTELTBACKDIV  */
  YYSYMBOL_75_ = 75,                       /* '%'  */
  YYSYMBOL_76_ = 76,                       /* '\''  */
  YYSYMBOL_UMINUS = 77,                    /* UMINUS  */
  YYSYMBOL_UPLUS = 78,                     /* UPLUS  */
  YYSYMBOL_79_ = 79,                       /* '^'  */
  YYSYMBOL_80_ = 80,                       /* '!'  */
  YYSYMBOL_81_n_ = 81,                     /* '\n'  */
  YYSYMBOL_82_ = 82,                       /* '('  */
  YYSYMBOL_83_ = 83,                       /* ')'  */
  YYSYMBOL_84_ = 84,                       /* '|'  */
  YYSYMBOL_85_ = 85,                       /* ','  */
  YYSYMBOL_86_ = 86,                       /* '['  */
  YYSYMBOL_87_ = 87,                       /* ']'  */
  YYSYMBOL_88_ = 88,                       /* '`'  */
  YYSYMBOL_89_ = 89,                       /* '{'  */
  YYSYMBOL_90_ = 90,                       /* '}'  */
  YYSYMBOL_91_ = 91,                       /* '&'  */
  YYSYMBOL_92_ = 92,                       /* '.'  */
  YYSYMBOL_YYACCEPT = 93,                  /* $accept  */
  YYSYMBOL_fullexpr = 94,                  /* fullexpr  */
  YYSYMBOL_expr = 95,                      /* expr  */
  YYSYMBOL_deref = 96,                     /* deref  */
  YYSYMBOL_ident = 97,                     /* ident  */
  YYSYMBOL_paramdef = 98,                  /* paramdef  */
  YYSYMBOL_anyequals = 99,                 /* anyequals  */
  YYSYMBOL_funcdef = 100,                  /* funcdef  */
  YYSYMBOL_identlist = 101,                /* identlist  */
  YYSYMBOL_exprlist = 102,                 /* exprlist  */
  YYSYMBOL_matrixrows = 103                /* matrixrows  */
};
typedef enum yysymbol_kind_t yysymbol_kind_t;




#ifdef short
# undef short
#endif

/* On compilers that do not define __PTRDIFF_MAX__ etc., make sure
   <limits.h> and (if available) <stdint.h> are included
   so that the code can choose integer types of a good width.  */

#ifndef __PTRDIFF_MAX__
# include <limits.h> /* INFRINGES ON USER NAME SPACE */
# if defined __STDC_VERSION__ && 199901 <= __STDC_VERSION__
#  include <stdint.h> /* INFRINGES ON USER NAME SPACE */
#  define YY_STDINT_H
# endif
#endif

/* Narrow types that promote to a signed type and that can represent a
   signed or unsigned integer of at least N bits.  In tables they can
   save space and decrease cache pressure.  Promoting to a signed type
   helps avoid bugs in integer arithmetic.  */

#ifdef __INT_LEAST8_MAX__
typedef __INT_LEAST8_TYPE__ yytype_int8;
#elif defined YY_STDINT_H
typedef int_least8_t yytype_int8;
#else
typedef signed char yytype_int8;
#endif

#ifdef __INT_LEAST16_MAX__
typedef __INT_LEAST16_TYPE__ yytype_int16;
#elif defined YY_STDINT_H
typedef int_least16_t yytype_int16;
#else
typedef short yytype_int16;
#endif

#if defined __UINT_LEAST8_MAX__ && __UINT_LEAST8_MAX__ <= __INT_MAX__
typedef __UINT_LEAST8_TYPE__ yytype_uint8;
#elif (!defined __UINT_LEAST8_MAX__ && defined YY_STDINT_H \
       && UINT_LEAST8_MAX <= INT_MAX)
typedef uint_least8_t yytype_uint8;
#elif !defined __UINT_LEAST8_MAX__ && UCHAR_MAX <= INT_MAX
typedef unsigned char yytype_uint8;
#else
typedef short yytype_uint8;
#endif

#if defined __UINT_LEAST16_MAX__ && __UINT_LEAST16_MAX__ <= __INT_MAX__
typedef __UINT_LEAST16_TYPE__ yytype_uint16;
#elif (!defined __UINT_LEAST16_MAX__ && defined YY_STDINT_H \
       && UINT_LEAST16_MAX <= INT_MAX)
typedef uint_least16_t yytype_uint16;
#elif !defined __UINT_LEAST16_MAX__ && USHRT_MAX <= INT_MAX
typedef unsigned short yytype_uint16;
#else
typedef int yytype_uint16;
#endif

#ifndef YYPTRDIFF_T
# if defined __PTRDIFF_TYPE__ && defined __PTRDIFF_MAX__
#  define YYPTRDIFF_T __PTRDIFF_TYPE__
#  define YYPTRDIFF_MAXIMUM __PTRDIFF_MAX__
# elif defined PTRDIFF_MAX
#  ifndef ptrdiff_t
#   include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  endif
#  define YYPTRDIFF_T ptrdiff_t
#  define YYPTRDIFF_MAXIMUM PTRDIFF_MAX
# else
#  define YYPTRDIFF_T long
#  define YYPTRDIFF_MAXIMUM LONG_MAX
# endif
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif defined __STDC_VERSION__ && 199901 <= __STDC_VERSION__
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned
# endif
#endif

#define YYSIZE_MAXIMUM                                  \
  YY_CAST (YYPTRDIFF_T,                                 \
           (YYPTRDIFF_MAXIMUM < YY_CAST (YYSIZE_T, -1)  \
            ? YYPTRDIFF_MAXIMUM                         \
            : YY_CAST (YYSIZE_T, -1)))

#define YYSIZEOF(X) YY_CAST (YYPTRDIFF_T, sizeof (X))


/* Stored state numbers (used for stacks). */
typedef yytype_int16 yy_state_t;

/* State numbers in computations.  */
typedef int yy_state_fast_t;

#ifndef YY_
# if defined YYENABLE_NLS && YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(Msgid) dgettext ("bison-runtime", Msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(Msgid) Msgid
# endif
#endif


#ifndef YY_ATTRIBUTE_PURE
# if defined __GNUC__ && 2 < __GNUC__ + (96 <= __GNUC_MINOR__)
#  define YY_ATTRIBUTE_PURE __attribute__ ((__pure__))
# else
#  define YY_ATTRIBUTE_PURE
# endif
#endif

#ifndef YY_ATTRIBUTE_UNUSED
# if defined __GNUC__ && 2 < __GNUC__ + (7 <= __GNUC_MINOR__)
#  define YY_ATTRIBUTE_UNUSED __attribute__ ((__unused__))
# else
#  define YY_ATTRIBUTE_UNUSED
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YYUSE(E) ((void) (E))
#else
# define YYUSE(E) /* empty */
#endif

#if defined __GNUC__ && ! defined __ICC && 407 <= __GNUC__ * 100 + __GNUC_MINOR__
/* Suppress an incorrect diagnostic about yylval being uninitialized.  */
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN                            \
    _Pragma ("GCC diagnostic push")                                     \
    _Pragma ("GCC diagnostic ignored \"-Wuninitialized\"")              \
    _Pragma ("GCC diagnostic ignored \"-Wmaybe-uninitialized\"")
# define YY_IGNORE_MAYBE_UNINITIALIZED_END      \
    _Pragma ("GCC diagnostic pop")
#else
# define YY_INITIAL_VALUE(Value) Value
#endif
#ifndef YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_END
#endif
#ifndef YY_INITIAL_VALUE
# define YY_INITIAL_VALUE(Value) /* Nothing. */
#endif

#if defined __cplusplus && defined __GNUC__ && ! defined __ICC && 6 <= __GNUC__
# define YY_IGNORE_USELESS_CAST_BEGIN                          \
    _Pragma ("GCC diagnostic push")                            \
    _Pragma ("GCC diagnostic ignored \"-Wuseless-cast\"")
# define YY_IGNORE_USELESS_CAST_END            \
    _Pragma ("GCC diagnostic pop")
#endif
#ifndef YY_IGNORE_USELESS_CAST_BEGIN
# define YY_IGNORE_USELESS_CAST_BEGIN
# define YY_IGNORE_USELESS_CAST_END
#endif


#define YY_ASSERT(E) ((void) (0 && (E)))

#if !defined yyoverflow

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   elif defined __BUILTIN_VA_ARG_INCR
#    include <alloca.h> /* INFRINGES ON USER NAME SPACE */
#   elif defined _AIX
#    define YYSTACK_ALLOC __alloca
#   elif defined _MSC_VER
#    include <malloc.h> /* INFRINGES ON USER NAME SPACE */
#    define alloca _alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if ! defined _ALLOCA_H && ! defined EXIT_SUCCESS
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
      /* Use EXIT_SUCCESS as a witness for stdlib.h.  */
#     ifndef EXIT_SUCCESS
#      define EXIT_SUCCESS 0
#     endif
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's 'empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (0)
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined EXIT_SUCCESS \
       && ! ((defined YYMALLOC || defined malloc) \
             && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef EXIT_SUCCESS
#    define EXIT_SUCCESS 0
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined EXIT_SUCCESS
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined EXIT_SUCCESS
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
#endif /* !defined yyoverflow */

#if (! defined yyoverflow \
     && (! defined __cplusplus \
         || (defined YYSTYPE_IS_TRIVIAL && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  yy_state_t yyss_alloc;
  YYSTYPE yyvs_alloc;
};

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (YYSIZEOF (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (YYSIZEOF (yy_state_t) + YYSIZEOF (YYSTYPE)) \
      + YYSTACK_GAP_MAXIMUM)

# define YYCOPY_NEEDED 1

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack_alloc, Stack)                           \
    do                                                                  \
      {                                                                 \
        YYPTRDIFF_T yynewbytes;                                         \
        YYCOPY (&yyptr->Stack_alloc, Stack, yysize);                    \
        Stack = &yyptr->Stack_alloc;                                    \
        yynewbytes = yystacksize * YYSIZEOF (*Stack) + YYSTACK_GAP_MAXIMUM; \
        yyptr += yynewbytes / YYSIZEOF (*yyptr);                        \
      }                                                                 \
    while (0)

#endif

#if defined YYCOPY_NEEDED && YYCOPY_NEEDED
/* Copy COUNT objects from SRC to DST.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(Dst, Src, Count) \
      __builtin_memcpy (Dst, Src, YY_CAST (YYSIZE_T, (Count)) * sizeof (*(Src)))
#  else
#   define YYCOPY(Dst, Src, Count)              \
      do                                        \
        {                                       \
          YYPTRDIFF_T yyi;                      \
          for (yyi = 0; yyi < (Count); yyi++)   \
            (Dst)[yyi] = (Src)[yyi];            \
        }                                       \
      while (0)
#  endif
# endif
#endif /* !YYCOPY_NEEDED */

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  48
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   2515

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  93
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  11
/* YYNRULES -- Number of rules.  */
#define YYNRULES  130
/* YYNSTATES -- Number of states.  */
#define YYNSTATES  310

/* YYMAXUTOK -- Last valid token kind.  */
#define YYMAXUTOK   325


/* YYTRANSLATE(TOKEN-NUM) -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex, with out-of-bounds checking.  */
#define YYTRANSLATE(YYX)                                \
  (0 <= (YYX) && (YYX) <= YYMAXUTOK                     \
   ? YY_CAST (yysymbol_kind_t, yytranslate[YYX])        \
   : YYSYMBOL_YYUNDEF)

/* YYTRANSLATE[TOKEN-NUM] -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex.  */
static const yytype_int8 yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
      81,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,    80,     2,     2,     2,    75,    91,    76,
      82,    83,    71,    69,    85,    70,    92,    72,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,    68,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,    86,    73,    87,    79,     2,    88,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,    89,    84,    90,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    40,    41,    42,    43,    44,
      45,    46,    47,    48,    49,    50,    51,    52,    53,    54,
      55,    56,    57,    58,    59,    60,    61,    62,    63,    64,
      65,    66,    67,    74,    77,    78
};

#if YYDEBUG
  /* YYRLINE[YYN] -- Source line where rule number YYN was defined.  */
static const yytype_int16 yyrline[] =
{
       0,   121,   121,   122,   123,   124,   125,   126,   127,   128,
     129,   130,   131,   132,   133,   134,   137,   138,   142,   146,
     147,   149,   150,   157,   158,   159,   160,   161,   162,   163,
     164,   165,   166,   167,   168,   169,   170,   171,   172,   173,
     174,   175,   177,   178,   179,   180,   181,   182,   184,   185,
     186,   187,   192,   193,   194,   195,   196,   197,   198,   199,
     201,   209,   211,   212,   213,   214,   215,   216,   217,   218,
     225,   230,   231,   232,   233,   234,   235,   236,   237,   238,
     239,   240,   241,   242,   243,   244,   245,   246,   247,   249,
     250,   251,   252,   254,   255,   257,   258,   259,   261,   262,
     263,   264,   265,   266,   267,   268,   269,   270,   272,   273,
     276,   279,   282,   292,   293,   296,   304,   312,   320,   328,
     336,   345,   353,   361,   371,   372,   375,   378,   379,   382,
     383
};
#endif

/** Accessing symbol of state STATE.  */
#define YY_ACCESSING_SYMBOL(State) YY_CAST (yysymbol_kind_t, yystos[State])

#if YYDEBUG || 0
/* The user-facing name of the symbol whose (internal) number is
   YYSYMBOL.  No bounds checking.  */
static const char *yysymbol_name (yysymbol_kind_t yysymbol) YY_ATTRIBUTE_UNUSED;

/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals.  */
static const char *const yytname[] =
{
  "\"end of file\"", "error", "\"invalid token\"", "STARTTOK", "LOADFILE",
  "LOADFILE_GLOB", "LOAD_PLUGIN", "CHANGEDIR", "PWD", "LS", "LS_ARG",
  "HELP", "HELP_ARG", "NUMBER", "STRING", "FUNCID", "FUNCTION", "CALL",
  "THREEDOTS", "PARAMETER", "RETURNTOK", "BAILOUT", "EXCEPTION",
  "CONTINUE", "BREAK", "LOCAL", "WHILE", "UNTIL", "FOR", "SUM", "PROD",
  "DO", "IF", "THEN", "ELSE", "TO", "BY", "IN", "AT", "MAKEIMAGPARENTH",
  "SEPAR", "NEXTROW", "EQUALS", "DEFEQUALS", "SWAPWITH", "TRANSPOSE",
  "ELTELTDIV", "ELTELTMUL", "ELTELTPLUS", "ELTELTMINUS", "ELTELTEXP",
  "ELTELTMOD", "DOUBLEFACT", "EQ_CMP", "NE_CMP", "CMP_CMP", "LT_CMP",
  "GT_CMP", "LE_CMP", "GE_CMP", "LOGICAL_XOR", "LOGICAL_OR", "LOGICAL_AND",
  "LOGICAL_NOT", "INCREMENT", "LOWER_THAN_ELSE", "LOWER_THAN_INCREMENT",
  "MOD", "':'", "'+'", "'-'", "'*'", "'/'", "'\\\\'", "ELTELTBACKDIV",
  "'%'", "'\\''", "UMINUS", "UPLUS", "'^'", "'!'", "'\\n'", "'('", "')'",
  "'|'", "','", "'['", "']'", "'`'", "'{'", "'}'", "'&'", "'.'", "$accept",
  "fullexpr", "expr", "deref", "ident", "paramdef", "anyequals", "funcdef",
  "identlist", "exprlist", "matrixrows", YY_NULLPTR
};

static const char *
yysymbol_name (yysymbol_kind_t yysymbol)
{
  return yytname[yysymbol];
}
#endif

#ifdef YYPRINT
/* YYTOKNUM[NUM] -- (External) token number corresponding to the
   (internal) symbol number NUM (which must be that of a token).  */
static const yytype_int16 yytoknum[] =
{
       0,   256,   257,   258,   259,   260,   261,   262,   263,   264,
     265,   266,   267,   268,   269,   270,   271,   272,   273,   274,
     275,   276,   277,   278,   279,   280,   281,   282,   283,   284,
     285,   286,   287,   288,   289,   290,   291,   292,   293,   294,
     295,   296,   297,   298,   299,   300,   301,   302,   303,   304,
     305,   306,   307,   308,   309,   310,   311,   312,   313,   314,
     315,   316,   317,   318,   319,   320,   321,   322,    58,    43,
      45,    42,    47,    92,   323,    37,    39,   324,   325,    94,
      33,    10,    40,    41,   124,    44,    91,    93,    96,   123,
     125,    38,    46
};
#endif

#define YYPACT_NINF (-72)

#define yypact_value_is_default(Yyn) \
  ((Yyn) == YYPACT_NINF)

#define YYTABLE_NINF (-1)

#define yytable_value_is_error(Yyn) \
  ((Yyn) == YYTABLE_NINF)

  /* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
     STATE-NUM.  */
static const yytype_int16 yypact[] =
{
      65,   -69,   300,    40,   -72,   -31,   -18,     4,     8,    21,
      44,    50,    51,    76,   -72,   -72,   -72,    -8,    42,   820,
     -72,   -72,   -72,   -72,    -4,   820,   820,    42,    42,    42,
     820,   820,   820,   820,   820,   820,    42,   -72,   820,   820,
     820,   -10,   820,    42,   -72,  1198,    -9,     2,   -72,   -72,
     -72,   -72,   -72,   -72,   -72,   -72,   -72,   -72,     0,    79,
     -72,    35,   -72,  2135,   131,   -72,   -24,  1442,  1488,     5,
      12,    85,  1258,  1536,  2224,  1996,   127,   127,   -72,   968,
      82,   921,  2042,    87,   -28,   820,   820,   -72,   -72,   -71,
     -72,   100,   330,   440,   820,   820,   820,   -72,   820,   820,
     820,   820,   820,   820,   -72,   820,   820,   820,   820,   820,
     820,   820,   820,   820,   820,   820,   820,   820,   820,   820,
     820,   820,   820,   820,   -72,   820,   -72,   -72,   520,   550,
     -21,   -14,   -72,   -72,   -72,   820,   820,   820,   820,    42,
     820,   820,   820,   820,   820,   820,   820,   820,   820,   820,
     820,   820,   -72,   630,   -72,   710,   -72,   710,   820,   -72,
     -23,   -65,   -72,   740,     3,   820,   875,   -72,  2135,  2267,
    2267,  2310,   124,   124,   631,   631,   127,   124,  2396,  2396,
    2353,  2396,  2396,  2396,  2396,  2181,  2181,  2224,  2267,  2435,
     631,   631,   124,   124,   124,   124,   124,   127,   -72,    41,
     -72,    69,    -7,   820,    92,   -16,  2267,  2135,  2135,   -72,
    2135,  2135,  1582,  1628,  1674,  1720,  1766,  1812,  2135,  2135,
    2089,  2135,   -72,   820,  1014,  2042,    87,   -72,   -72,   -72,
      75,   -72,   820,  1060,   -72,   410,   -72,   -72,    35,    81,
    2135,     9,    -6,   820,   820,   820,   820,   820,   820,   820,
     820,  2042,   -72,   -72,  1106,   -72,   101,   -72,  1152,   820,
      35,    -5,   820,    35,    93,  2135,  2135,  1304,  2135,  1350,
    2135,  1396,  2135,   -72,   -72,   -72,  2135,   820,    35,    96,
    2135,   820,    35,   820,   820,   820,   820,   820,   820,  2135,
     820,    35,  2135,   820,  2135,  1858,  2135,  1904,  2135,  1950,
    2135,   820,  2135,   820,   820,   820,  2135,  2135,  2135,  2135
};

  /* YYDEFACT[STATE-NUM] -- Default reduction number in state STATE-NUM.
     Performed when YYTABLE does not specify something else to do.  Zero
     means the default is an error.  */
static const yytype_uint8 yydefact[] =
{
       0,    15,     0,     0,    14,     0,     0,     0,     0,     0,
       0,     0,     0,     0,   107,   108,   111,     0,     0,     0,
     103,   104,   105,   106,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,    12,     0,     0,
       0,     0,     0,     0,   109,     0,    91,    88,     1,     3,
       4,    11,     5,    10,     6,     7,     8,     9,     0,     0,
     100,     0,    99,   102,     0,   125,     0,     0,     0,     0,
       0,     0,     0,     0,    51,    25,    57,    56,   110,   128,
       0,     0,   128,   130,     0,     0,     0,    89,   101,     0,
      90,     0,     0,     0,     0,     0,     0,    55,     0,     0,
       0,     0,     0,     0,    53,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,    54,     0,    52,     2,     0,     0,
       0,     0,    98,   113,   114,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,    22,     0,    21,     0,    28,     0,     0,    68,
       0,     0,    71,     0,     0,     0,     0,    13,    16,    23,
      24,    27,    36,    34,    30,    32,    59,    40,    42,    43,
      41,    44,    45,    46,    47,    50,    49,    48,    19,    60,
      29,    31,    33,    35,    37,    38,    39,    58,    94,     0,
      92,     0,     0,     0,     0,     0,   112,    17,    18,   124,
      73,    74,     0,     0,     0,     0,     0,     0,    75,    76,
      86,    26,    20,     0,   126,   126,   129,    69,    72,    97,
       0,    61,     0,     0,    62,     0,    95,    93,     0,     0,
     117,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,   127,    70,    96,     0,    66,     0,    64,     0,     0,
       0,     0,     0,     0,     0,   115,    79,     0,    82,     0,
      85,     0,    87,    67,    65,    63,   123,     0,     0,     0,
     116,     0,     0,     0,     0,     0,     0,     0,     0,   120,
       0,     0,   121,     0,    77,     0,    80,     0,    83,     0,
     122,     0,   118,     0,     0,     0,   119,    78,    81,    84
};

  /* YYPGOTO[NTERM-NUM].  */
static const yytype_int8 yypgoto[] =
{
     -72,   -72,    -2,   -72,    17,   -72,   -68,    -3,   -52,     1,
     102
};

  /* YYDEFGOTO[NTERM-NUM].  */
static const yytype_int16 yydefgoto[] =
{
      -1,     3,    82,    46,    47,    62,   135,    60,    66,    83,
      84
};

  /* YYTABLE[YYPACT[STATE-NUM]] -- What to do in state STATE-NUM.  If
     positive, shift that token.  If negative, reduce the rule whose
     number is the opposite.  If YYTABLE_NINF, syntax error.  */
static const yytype_int16 yytable[] =
{
      45,   143,   145,   147,   204,    16,   131,    16,    16,    16,
      16,    16,     4,   158,   157,    16,   138,    63,   158,   162,
     157,   133,   134,    67,    68,   228,   133,   134,    72,    73,
      74,    75,    76,    77,    59,    61,    79,    81,    88,    80,
      48,    65,   142,    89,    69,    70,    71,   133,   134,   144,
      49,   133,   134,    78,   133,   134,   132,    16,    87,   159,
      90,   139,   203,    50,   227,   202,     1,    64,     2,   205,
     242,   139,    58,   128,    58,    65,    85,   133,   134,    86,
     238,   263,   278,   130,   129,    51,   231,   161,   232,    52,
     166,   168,   169,   170,   171,   261,   172,   173,   174,   175,
     176,   177,    53,   178,   179,   180,   181,   182,   183,   184,
     185,   186,   187,   188,   189,   190,   191,   192,   193,   194,
     195,   196,   146,   197,   236,    54,   157,   133,   134,   199,
     201,    55,    56,   206,   168,   207,   208,   243,   210,   211,
     212,   213,   214,   215,   216,   217,   218,   219,   220,   221,
     239,   168,   237,   224,   157,   225,   209,    57,   253,   226,
     157,    58,    92,   233,   230,    92,   139,   155,   260,    97,
     259,   137,   157,   262,   102,   241,   104,   102,   139,   104,
     282,   139,   163,   291,   274,     0,     0,   160,     0,     0,
     264,     0,   277,     0,     0,   281,     0,     0,     0,     0,
     124,   240,     0,   125,   126,     0,   125,   126,     0,   279,
     290,     0,     0,     0,   293,     0,     0,     0,     0,    65,
       0,   251,     0,   301,     0,     0,     0,     0,     0,     0,
     254,     0,     0,   258,     0,     0,     0,     0,     0,     0,
       0,   265,   266,   267,   268,   269,   270,   271,   272,     0,
       0,     0,     0,     0,     0,     0,     0,   276,     0,    65,
     280,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,   289,     0,     0,    65,   292,
       0,   294,   295,   296,   297,   298,   299,     0,   300,     0,
       0,   302,     0,     0,     0,     0,     0,     0,     0,   306,
       0,   307,   308,   309,     5,     6,     7,     8,     9,    10,
      11,    12,    13,    14,    15,    16,    17,     0,     0,    18,
      19,    20,    21,    22,    23,    24,    25,    26,    27,    28,
      29,    30,    31,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,    14,    15,    16,    17,     0,     0,    18,
      19,    20,    21,    22,    23,    24,    25,    26,    27,    28,
      29,    30,    31,    32,    33,     0,     0,     0,     0,    34,
      35,    36,     0,     0,     0,     0,     0,     0,     0,     0,
       0,    37,    38,     0,    39,     0,    40,     0,    41,    42,
       0,    43,    44,    32,    33,     0,     0,     0,   164,    34,
      35,    36,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,    38,     0,    39,   165,    40,     0,    41,    42,
       0,    43,    44,    14,    15,    16,    17,     0,     0,    18,
      19,    20,    21,    22,    23,    24,    25,    26,    27,    28,
      29,    30,    31,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,    14,    15,    16,    17,     0,     0,    18,
      19,    20,    21,    22,    23,    24,    25,    26,    27,    28,
      29,    30,    31,    32,    33,     0,     0,     0,   256,    34,
      35,    36,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,    38,   257,    39,     0,    40,     0,    41,    42,
       0,    43,    44,    32,    33,     0,     0,     0,     0,    34,
      35,    36,     0,     0,     0,     0,     0,     0,     0,     0,
       0,   167,    38,     0,    39,     0,    40,     0,    41,    42,
       0,    43,    44,    14,    15,    16,    17,     0,     0,    18,
      19,    20,    21,    22,    23,    24,    25,    26,    27,    28,
      29,    30,    31,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,    14,    15,    16,    17,     0,     0,    18,
      19,    20,    21,    22,    23,    24,    25,    26,    27,    28,
      29,    30,    31,    32,    33,     0,     0,     0,     0,    34,
      35,    36,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,    38,   198,    39,     0,    40,     0,    41,    42,
       0,    43,    44,    32,    33,     0,     0,     0,     0,    34,
      35,    36,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,    38,   200,    39,     0,    40,     0,    41,    42,
       0,    43,    44,    14,    15,    16,    17,     0,     0,    18,
      19,    20,    21,    22,    23,    24,    25,    26,    27,    28,
      29,    30,    31,     0,     0,     0,     0,     0,     0,    92,
       0,     0,     0,     0,     0,     0,    97,    98,    99,     0,
       0,   102,   103,   104,     0,     0,     0,     0,     0,     0,
       0,     0,     0,    32,    33,     0,     0,     0,     0,    34,
      35,    36,   119,   120,   121,   122,   123,   124,     0,     0,
     125,   126,    38,   222,    39,     0,    40,     0,    41,    42,
       0,    43,    44,    14,    15,    16,    17,     0,     0,    18,
      19,    20,    21,    22,    23,    24,    25,    26,    27,    28,
      29,    30,    31,     0,     0,     0,     0,     0,     0,     0,
       0,   223,     0,    14,    15,    16,    17,     0,     0,    18,
      19,    20,    21,    22,    23,    24,    25,    26,    27,    28,
      29,    30,    31,    32,    33,     0,     0,     0,     0,    34,
      35,    36,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,    38,     0,    39,     0,    40,     0,    41,    42,
       0,    43,    44,    32,    33,     0,     0,     0,     0,    34,
      35,    36,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,    38,   229,    39,     0,    40,     0,    41,    42,
       0,    43,    44,    14,    15,    16,    17,     0,     0,    18,
      19,    20,    21,    22,    23,    24,    25,    26,    27,    28,
      29,    30,    31,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,    32,    33,     0,     0,     0,     0,    34,
      35,    36,    91,     0,     0,     0,     0,     0,     0,     0,
       0,     0,    38,     0,    39,     0,    40,     0,    41,    42,
       0,    43,    44,    92,     0,   136,     0,    94,    95,    96,
      97,    98,    99,   100,   101,   102,   103,   104,   105,   106,
     107,   108,   109,   110,   111,   112,   113,   114,    91,     0,
       0,     0,   115,   116,   117,   118,   119,   120,   121,   122,
     123,   124,     0,     0,   125,   126,     0,     0,   234,    92,
     235,   136,     0,    94,    95,    96,    97,    98,    99,   100,
     101,   102,   103,   104,   105,   106,   107,   108,   109,   110,
     111,   112,   113,   114,     0,    91,     0,     0,   115,   116,
     117,   118,   119,   120,   121,   122,   123,   124,     0,     0,
     125,   126,     0,     0,     0,   156,    92,   152,   153,     0,
      94,    95,    96,    97,    98,    99,   100,   101,   102,   103,
     104,   105,   106,   107,   108,   109,   110,   111,   112,   113,
     114,    91,     0,     0,     0,   115,   116,   117,   118,   119,
     120,   121,   122,   123,   124,     0,     0,   125,   126,     0,
       0,   154,    92,     0,   136,     0,    94,    95,    96,    97,
      98,    99,   100,   101,   102,   103,   104,   105,   106,   107,
     108,   109,   110,   111,   112,   113,   114,    91,     0,     0,
       0,   115,   116,   117,   118,   119,   120,   121,   122,   123,
     124,     0,     0,   125,   126,     0,     0,   252,    92,     0,
     136,     0,    94,    95,    96,    97,    98,    99,   100,   101,
     102,   103,   104,   105,   106,   107,   108,   109,   110,   111,
     112,   113,   114,    91,     0,     0,     0,   115,   116,   117,
     118,   119,   120,   121,   122,   123,   124,     0,     0,   125,
     126,     0,     0,   255,    92,     0,   136,     0,    94,    95,
      96,    97,    98,    99,   100,   101,   102,   103,   104,   105,
     106,   107,   108,   109,   110,   111,   112,   113,   114,    91,
       0,     0,     0,   115,   116,   117,   118,   119,   120,   121,
     122,   123,   124,     0,     0,   125,   126,     0,     0,   273,
      92,     0,   136,     0,    94,    95,    96,    97,    98,    99,
     100,   101,   102,   103,   104,   105,   106,   107,   108,   109,
     110,   111,   112,   113,   114,    91,     0,     0,     0,   115,
     116,   117,   118,   119,   120,   121,   122,   123,   124,     0,
       0,   125,   126,     0,     0,   275,    92,     0,    93,     0,
      94,    95,    96,    97,    98,    99,   100,   101,   102,   103,
     104,   105,   106,   107,   108,   109,   110,   111,   112,   113,
     114,     0,     0,     0,     0,   115,   116,   117,   118,   119,
     120,   121,   122,   123,   124,    91,     0,   125,   126,   127,
       0,     0,     0,     0,   148,   149,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,    92,     0,   136,     0,
      94,    95,    96,    97,    98,    99,   100,   101,   102,   103,
     104,   105,   106,   107,   108,   109,   110,   111,   112,   113,
     114,    91,     0,     0,     0,   115,   116,   117,   118,   119,
     120,   121,   122,   123,   124,   283,     0,   125,   126,     0,
     284,     0,    92,     0,   136,     0,    94,    95,    96,    97,
      98,    99,   100,   101,   102,   103,   104,   105,   106,   107,
     108,   109,   110,   111,   112,   113,   114,    91,     0,     0,
       0,   115,   116,   117,   118,   119,   120,   121,   122,   123,
     124,   285,     0,   125,   126,     0,   286,     0,    92,     0,
     136,     0,    94,    95,    96,    97,    98,    99,   100,   101,
     102,   103,   104,   105,   106,   107,   108,   109,   110,   111,
     112,   113,   114,    91,     0,     0,     0,   115,   116,   117,
     118,   119,   120,   121,   122,   123,   124,   287,     0,   125,
     126,     0,   288,     0,    92,     0,   136,     0,    94,    95,
      96,    97,    98,    99,   100,   101,   102,   103,   104,   105,
     106,   107,   108,   109,   110,   111,   112,   113,   114,    91,
       0,     0,     0,   115,   116,   117,   118,   119,   120,   121,
     122,   123,   124,   140,     0,   125,   126,     0,     0,     0,
      92,     0,   136,     0,    94,    95,    96,    97,    98,    99,
     100,   101,   102,   103,   104,   105,   106,   107,   108,   109,
     110,   111,   112,   113,   114,    91,     0,     0,     0,   115,
     116,   117,   118,   119,   120,   121,   122,   123,   124,   141,
       0,   125,   126,     0,     0,     0,    92,     0,   136,     0,
      94,    95,    96,    97,    98,    99,   100,   101,   102,   103,
     104,   105,   106,   107,   108,   109,   110,   111,   112,   113,
     114,     0,     0,    91,     0,   115,   116,   117,   118,   119,
     120,   121,   122,   123,   124,     0,     0,   125,   126,   150,
       0,     0,     0,     0,    92,     0,   136,     0,    94,    95,
      96,    97,    98,    99,   100,   101,   102,   103,   104,   105,
     106,   107,   108,   109,   110,   111,   112,   113,   114,    91,
       0,     0,     0,   115,   116,   117,   118,   119,   120,   121,
     122,   123,   124,   244,     0,   125,   126,     0,     0,     0,
      92,     0,   136,     0,    94,    95,    96,    97,    98,    99,
     100,   101,   102,   103,   104,   105,   106,   107,   108,   109,
     110,   111,   112,   113,   114,    91,     0,     0,     0,   115,
     116,   117,   118,   119,   120,   121,   122,   123,   124,     0,
       0,   125,   126,   245,     0,     0,    92,     0,   136,     0,
      94,    95,    96,    97,    98,    99,   100,   101,   102,   103,
     104,   105,   106,   107,   108,   109,   110,   111,   112,   113,
     114,    91,     0,     0,     0,   115,   116,   117,   118,   119,
     120,   121,   122,   123,   124,   246,     0,   125,   126,     0,
       0,     0,    92,     0,   136,     0,    94,    95,    96,    97,
      98,    99,   100,   101,   102,   103,   104,   105,   106,   107,
     108,   109,   110,   111,   112,   113,   114,    91,     0,     0,
       0,   115,   116,   117,   118,   119,   120,   121,   122,   123,
     124,     0,     0,   125,   126,   247,     0,     0,    92,     0,
     136,     0,    94,    95,    96,    97,    98,    99,   100,   101,
     102,   103,   104,   105,   106,   107,   108,   109,   110,   111,
     112,   113,   114,    91,     0,     0,     0,   115,   116,   117,
     118,   119,   120,   121,   122,   123,   124,   248,     0,   125,
     126,     0,     0,     0,    92,     0,   136,     0,    94,    95,
      96,    97,    98,    99,   100,   101,   102,   103,   104,   105,
     106,   107,   108,   109,   110,   111,   112,   113,   114,    91,
       0,     0,     0,   115,   116,   117,   118,   119,   120,   121,
     122,   123,   124,     0,     0,   125,   126,   249,     0,     0,
      92,     0,   136,     0,    94,    95,    96,    97,    98,    99,
     100,   101,   102,   103,   104,   105,   106,   107,   108,   109,
     110,   111,   112,   113,   114,    91,     0,     0,     0,   115,
     116,   117,   118,   119,   120,   121,   122,   123,   124,   303,
       0,   125,   126,     0,     0,     0,    92,     0,   136,     0,
      94,    95,    96,    97,    98,    99,   100,   101,   102,   103,
     104,   105,   106,   107,   108,   109,   110,   111,   112,   113,
     114,    91,     0,     0,     0,   115,   116,   117,   118,   119,
     120,   121,   122,   123,   124,   304,     0,   125,   126,     0,
       0,     0,    92,     0,   136,     0,    94,    95,    96,    97,
      98,    99,   100,   101,   102,   103,   104,   105,   106,   107,
     108,   109,   110,   111,   112,   113,   114,    91,     0,     0,
       0,   115,   116,   117,   118,   119,   120,   121,   122,   123,
     124,   305,     0,   125,   126,     0,     0,     0,    92,     0,
     136,     0,    94,    95,    96,    97,    98,    99,   100,   101,
     102,   103,   104,   105,   106,   107,   108,   109,   110,   111,
     112,   113,   114,    91,     0,     0,     0,   115,   116,   117,
     118,   119,   120,   121,   122,   123,   124,     0,     0,   125,
     126,     0,   151,     0,    92,     0,     0,     0,    94,    95,
      96,    97,    98,    99,   100,   101,   102,   103,   104,   105,
     106,   107,   108,   109,   110,   111,   112,   113,   114,    91,
       0,     0,     0,   115,   116,   117,   118,   119,   120,   121,
     122,   123,   124,     0,     0,   125,   126,     0,     0,     0,
      92,     0,   136,     0,    94,    95,    96,    97,    98,    99,
     100,   101,   102,   103,   104,   105,   106,   107,   108,   109,
     110,   111,   112,   113,   114,     0,    91,     0,     0,   115,
     116,   117,   118,   119,   120,   121,   122,   123,   124,     0,
       0,   125,   126,   250,     0,     0,     0,    92,     0,     0,
       0,    94,    95,    96,    97,    98,    99,   100,   101,   102,
     103,   104,   105,   106,   107,   108,   109,   110,   111,   112,
     113,   114,    91,     0,     0,     0,   115,   116,   117,   118,
     119,   120,   121,   122,   123,   124,     0,     0,   125,   126,
       0,     0,     0,    92,     0,     0,     0,    94,    95,    96,
      97,    98,    99,   100,   101,   102,   103,   104,   105,   106,
     107,   108,   109,   110,   111,   112,   113,   114,    91,     0,
       0,     0,   115,   116,   117,   118,   119,   120,   121,   122,
     123,   124,     0,     0,   125,   126,     0,     0,     0,    92,
       0,     0,     0,    94,    95,    96,    97,    98,    99,   100,
     101,   102,   103,   104,   105,   106,   107,   108,   109,   110,
     111,    91,     0,   114,     0,     0,     0,     0,   115,   116,
     117,   118,   119,   120,   121,   122,   123,   124,     0,     0,
     125,   126,    92,     0,     0,     0,    94,    95,    96,    97,
      98,    99,   100,   101,   102,   103,   104,   105,   106,   107,
     108,   109,   110,   111,    91,     0,     0,     0,     0,     0,
       0,   115,   116,   117,   118,   119,   120,   121,   122,   123,
     124,     0,     0,   125,   126,    92,     0,     0,     0,    94,
      95,    96,    97,    98,    99,   100,   101,   102,   103,   104,
     105,   106,   107,   108,   109,   110,   111,    91,     0,     0,
       0,     0,     0,     0,     0,   116,   117,   118,   119,   120,
     121,   122,   123,   124,     0,     0,   125,   126,    92,     0,
       0,     0,     0,     0,    -1,    97,    98,    99,   100,   101,
     102,   103,   104,   105,   106,   107,   108,   109,   110,   111,
      91,     0,     0,     0,     0,     0,     0,     0,   116,   117,
     118,   119,   120,   121,   122,   123,   124,     0,     0,   125,
     126,    92,     0,     0,     0,     0,     0,     0,    97,    98,
      99,   100,   101,   102,   103,   104,   105,   106,    -1,   108,
     109,   110,   111,    91,     0,     0,     0,     0,     0,     0,
       0,   116,   117,   118,   119,   120,   121,   122,   123,   124,
       0,     0,   125,   126,    92,     0,     0,     0,     0,     0,
       0,    97,    98,    99,   100,   101,   102,   103,   104,   105,
     106,     0,   108,   109,   110,   111,     0,     0,     0,     0,
       0,     0,     0,     0,   116,   117,   118,   119,   120,   121,
     122,   123,   124,    92,     0,   125,   126,     0,     0,     0,
      97,    98,    99,   100,   101,   102,   103,   104,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,   116,   117,   118,   119,   120,   121,   122,
     123,   124,     0,     0,   125,   126
};

static const yytype_int16 yycheck[] =
{
       2,    69,    70,    71,    18,    15,    58,    15,    15,    15,
      15,    15,    81,    41,    85,    15,    40,    19,    41,    90,
      85,    42,    43,    25,    26,    90,    42,    43,    30,    31,
      32,    33,    34,    35,    17,    18,    38,    39,    41,    38,
       0,    24,    37,    42,    27,    28,    29,    42,    43,    37,
      81,    42,    43,    36,    42,    43,    59,    15,    41,    87,
      43,    85,   130,    81,    87,    86,     1,    71,     3,    83,
      86,    85,    82,    82,    82,    58,    86,    42,    43,    89,
      87,    87,    87,    83,    82,    81,    83,    86,    85,    81,
      92,    93,    94,    95,    96,    86,    98,    99,   100,   101,
     102,   103,    81,   105,   106,   107,   108,   109,   110,   111,
     112,   113,   114,   115,   116,   117,   118,   119,   120,   121,
     122,   123,    37,   125,    83,    81,    85,    42,    43,   128,
     129,    81,    81,   135,   136,   137,   138,   205,   140,   141,
     142,   143,   144,   145,   146,   147,   148,   149,   150,   151,
     202,   153,    83,   155,    85,   157,   139,    81,    83,   158,
      85,    82,    38,   165,   163,    38,    85,    85,    87,    45,
     238,    40,    85,   241,    50,    83,    52,    50,    85,    52,
      87,    85,    82,    87,    83,    -1,    -1,    85,    -1,    -1,
     242,    -1,   260,    -1,    -1,   263,    -1,    -1,    -1,    -1,
      76,   203,    -1,    79,    80,    -1,    79,    80,    -1,   261,
     278,    -1,    -1,    -1,   282,    -1,    -1,    -1,    -1,   202,
      -1,   223,    -1,   291,    -1,    -1,    -1,    -1,    -1,    -1,
     232,    -1,    -1,   235,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,   243,   244,   245,   246,   247,   248,   249,   250,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,   259,    -1,   242,
     262,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,   277,    -1,    -1,   261,   281,
      -1,   283,   284,   285,   286,   287,   288,    -1,   290,    -1,
      -1,   293,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   301,
      -1,   303,   304,   305,     4,     5,     6,     7,     8,     9,
      10,    11,    12,    13,    14,    15,    16,    -1,    -1,    19,
      20,    21,    22,    23,    24,    25,    26,    27,    28,    29,
      30,    31,    32,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    13,    14,    15,    16,    -1,    -1,    19,
      20,    21,    22,    23,    24,    25,    26,    27,    28,    29,
      30,    31,    32,    63,    64,    -1,    -1,    -1,    -1,    69,
      70,    71,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    81,    82,    -1,    84,    -1,    86,    -1,    88,    89,
      -1,    91,    92,    63,    64,    -1,    -1,    -1,    68,    69,
      70,    71,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    82,    -1,    84,    85,    86,    -1,    88,    89,
      -1,    91,    92,    13,    14,    15,    16,    -1,    -1,    19,
      20,    21,    22,    23,    24,    25,    26,    27,    28,    29,
      30,    31,    32,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    13,    14,    15,    16,    -1,    -1,    19,
      20,    21,    22,    23,    24,    25,    26,    27,    28,    29,
      30,    31,    32,    63,    64,    -1,    -1,    -1,    68,    69,
      70,    71,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    82,    83,    84,    -1,    86,    -1,    88,    89,
      -1,    91,    92,    63,    64,    -1,    -1,    -1,    -1,    69,
      70,    71,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    81,    82,    -1,    84,    -1,    86,    -1,    88,    89,
      -1,    91,    92,    13,    14,    15,    16,    -1,    -1,    19,
      20,    21,    22,    23,    24,    25,    26,    27,    28,    29,
      30,    31,    32,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    13,    14,    15,    16,    -1,    -1,    19,
      20,    21,    22,    23,    24,    25,    26,    27,    28,    29,
      30,    31,    32,    63,    64,    -1,    -1,    -1,    -1,    69,
      70,    71,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    82,    83,    84,    -1,    86,    -1,    88,    89,
      -1,    91,    92,    63,    64,    -1,    -1,    -1,    -1,    69,
      70,    71,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    82,    83,    84,    -1,    86,    -1,    88,    89,
      -1,    91,    92,    13,    14,    15,    16,    -1,    -1,    19,
      20,    21,    22,    23,    24,    25,    26,    27,    28,    29,
      30,    31,    32,    -1,    -1,    -1,    -1,    -1,    -1,    38,
      -1,    -1,    -1,    -1,    -1,    -1,    45,    46,    47,    -1,
      -1,    50,    51,    52,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    63,    64,    -1,    -1,    -1,    -1,    69,
      70,    71,    71,    72,    73,    74,    75,    76,    -1,    -1,
      79,    80,    82,    83,    84,    -1,    86,    -1,    88,    89,
      -1,    91,    92,    13,    14,    15,    16,    -1,    -1,    19,
      20,    21,    22,    23,    24,    25,    26,    27,    28,    29,
      30,    31,    32,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    41,    -1,    13,    14,    15,    16,    -1,    -1,    19,
      20,    21,    22,    23,    24,    25,    26,    27,    28,    29,
      30,    31,    32,    63,    64,    -1,    -1,    -1,    -1,    69,
      70,    71,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    82,    -1,    84,    -1,    86,    -1,    88,    89,
      -1,    91,    92,    63,    64,    -1,    -1,    -1,    -1,    69,
      70,    71,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    82,    83,    84,    -1,    86,    -1,    88,    89,
      -1,    91,    92,    13,    14,    15,    16,    -1,    -1,    19,
      20,    21,    22,    23,    24,    25,    26,    27,    28,    29,
      30,    31,    32,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    63,    64,    -1,    -1,    -1,    -1,    69,
      70,    71,    17,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    82,    -1,    84,    -1,    86,    -1,    88,    89,
      -1,    91,    92,    38,    -1,    40,    -1,    42,    43,    44,
      45,    46,    47,    48,    49,    50,    51,    52,    53,    54,
      55,    56,    57,    58,    59,    60,    61,    62,    17,    -1,
      -1,    -1,    67,    68,    69,    70,    71,    72,    73,    74,
      75,    76,    -1,    -1,    79,    80,    -1,    -1,    83,    38,
      85,    40,    -1,    42,    43,    44,    45,    46,    47,    48,
      49,    50,    51,    52,    53,    54,    55,    56,    57,    58,
      59,    60,    61,    62,    -1,    17,    -1,    -1,    67,    68,
      69,    70,    71,    72,    73,    74,    75,    76,    -1,    -1,
      79,    80,    -1,    -1,    -1,    84,    38,    39,    40,    -1,
      42,    43,    44,    45,    46,    47,    48,    49,    50,    51,
      52,    53,    54,    55,    56,    57,    58,    59,    60,    61,
      62,    17,    -1,    -1,    -1,    67,    68,    69,    70,    71,
      72,    73,    74,    75,    76,    -1,    -1,    79,    80,    -1,
      -1,    83,    38,    -1,    40,    -1,    42,    43,    44,    45,
      46,    47,    48,    49,    50,    51,    52,    53,    54,    55,
      56,    57,    58,    59,    60,    61,    62,    17,    -1,    -1,
      -1,    67,    68,    69,    70,    71,    72,    73,    74,    75,
      76,    -1,    -1,    79,    80,    -1,    -1,    83,    38,    -1,
      40,    -1,    42,    43,    44,    45,    46,    47,    48,    49,
      50,    51,    52,    53,    54,    55,    56,    57,    58,    59,
      60,    61,    62,    17,    -1,    -1,    -1,    67,    68,    69,
      70,    71,    72,    73,    74,    75,    76,    -1,    -1,    79,
      80,    -1,    -1,    83,    38,    -1,    40,    -1,    42,    43,
      44,    45,    46,    47,    48,    49,    50,    51,    52,    53,
      54,    55,    56,    57,    58,    59,    60,    61,    62,    17,
      -1,    -1,    -1,    67,    68,    69,    70,    71,    72,    73,
      74,    75,    76,    -1,    -1,    79,    80,    -1,    -1,    83,
      38,    -1,    40,    -1,    42,    43,    44,    45,    46,    47,
      48,    49,    50,    51,    52,    53,    54,    55,    56,    57,
      58,    59,    60,    61,    62,    17,    -1,    -1,    -1,    67,
      68,    69,    70,    71,    72,    73,    74,    75,    76,    -1,
      -1,    79,    80,    -1,    -1,    83,    38,    -1,    40,    -1,
      42,    43,    44,    45,    46,    47,    48,    49,    50,    51,
      52,    53,    54,    55,    56,    57,    58,    59,    60,    61,
      62,    -1,    -1,    -1,    -1,    67,    68,    69,    70,    71,
      72,    73,    74,    75,    76,    17,    -1,    79,    80,    81,
      -1,    -1,    -1,    -1,    26,    27,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    38,    -1,    40,    -1,
      42,    43,    44,    45,    46,    47,    48,    49,    50,    51,
      52,    53,    54,    55,    56,    57,    58,    59,    60,    61,
      62,    17,    -1,    -1,    -1,    67,    68,    69,    70,    71,
      72,    73,    74,    75,    76,    31,    -1,    79,    80,    -1,
      36,    -1,    38,    -1,    40,    -1,    42,    43,    44,    45,
      46,    47,    48,    49,    50,    51,    52,    53,    54,    55,
      56,    57,    58,    59,    60,    61,    62,    17,    -1,    -1,
      -1,    67,    68,    69,    70,    71,    72,    73,    74,    75,
      76,    31,    -1,    79,    80,    -1,    36,    -1,    38,    -1,
      40,    -1,    42,    43,    44,    45,    46,    47,    48,    49,
      50,    51,    52,    53,    54,    55,    56,    57,    58,    59,
      60,    61,    62,    17,    -1,    -1,    -1,    67,    68,    69,
      70,    71,    72,    73,    74,    75,    76,    31,    -1,    79,
      80,    -1,    36,    -1,    38,    -1,    40,    -1,    42,    43,
      44,    45,    46,    47,    48,    49,    50,    51,    52,    53,
      54,    55,    56,    57,    58,    59,    60,    61,    62,    17,
      -1,    -1,    -1,    67,    68,    69,    70,    71,    72,    73,
      74,    75,    76,    31,    -1,    79,    80,    -1,    -1,    -1,
      38,    -1,    40,    -1,    42,    43,    44,    45,    46,    47,
      48,    49,    50,    51,    52,    53,    54,    55,    56,    57,
      58,    59,    60,    61,    62,    17,    -1,    -1,    -1,    67,
      68,    69,    70,    71,    72,    73,    74,    75,    76,    31,
      -1,    79,    80,    -1,    -1,    -1,    38,    -1,    40,    -1,
      42,    43,    44,    45,    46,    47,    48,    49,    50,    51,
      52,    53,    54,    55,    56,    57,    58,    59,    60,    61,
      62,    -1,    -1,    17,    -1,    67,    68,    69,    70,    71,
      72,    73,    74,    75,    76,    -1,    -1,    79,    80,    33,
      -1,    -1,    -1,    -1,    38,    -1,    40,    -1,    42,    43,
      44,    45,    46,    47,    48,    49,    50,    51,    52,    53,
      54,    55,    56,    57,    58,    59,    60,    61,    62,    17,
      -1,    -1,    -1,    67,    68,    69,    70,    71,    72,    73,
      74,    75,    76,    31,    -1,    79,    80,    -1,    -1,    -1,
      38,    -1,    40,    -1,    42,    43,    44,    45,    46,    47,
      48,    49,    50,    51,    52,    53,    54,    55,    56,    57,
      58,    59,    60,    61,    62,    17,    -1,    -1,    -1,    67,
      68,    69,    70,    71,    72,    73,    74,    75,    76,    -1,
      -1,    79,    80,    35,    -1,    -1,    38,    -1,    40,    -1,
      42,    43,    44,    45,    46,    47,    48,    49,    50,    51,
      52,    53,    54,    55,    56,    57,    58,    59,    60,    61,
      62,    17,    -1,    -1,    -1,    67,    68,    69,    70,    71,
      72,    73,    74,    75,    76,    31,    -1,    79,    80,    -1,
      -1,    -1,    38,    -1,    40,    -1,    42,    43,    44,    45,
      46,    47,    48,    49,    50,    51,    52,    53,    54,    55,
      56,    57,    58,    59,    60,    61,    62,    17,    -1,    -1,
      -1,    67,    68,    69,    70,    71,    72,    73,    74,    75,
      76,    -1,    -1,    79,    80,    35,    -1,    -1,    38,    -1,
      40,    -1,    42,    43,    44,    45,    46,    47,    48,    49,
      50,    51,    52,    53,    54,    55,    56,    57,    58,    59,
      60,    61,    62,    17,    -1,    -1,    -1,    67,    68,    69,
      70,    71,    72,    73,    74,    75,    76,    31,    -1,    79,
      80,    -1,    -1,    -1,    38,    -1,    40,    -1,    42,    43,
      44,    45,    46,    47,    48,    49,    50,    51,    52,    53,
      54,    55,    56,    57,    58,    59,    60,    61,    62,    17,
      -1,    -1,    -1,    67,    68,    69,    70,    71,    72,    73,
      74,    75,    76,    -1,    -1,    79,    80,    35,    -1,    -1,
      38,    -1,    40,    -1,    42,    43,    44,    45,    46,    47,
      48,    49,    50,    51,    52,    53,    54,    55,    56,    57,
      58,    59,    60,    61,    62,    17,    -1,    -1,    -1,    67,
      68,    69,    70,    71,    72,    73,    74,    75,    76,    31,
      -1,    79,    80,    -1,    -1,    -1,    38,    -1,    40,    -1,
      42,    43,    44,    45,    46,    47,    48,    49,    50,    51,
      52,    53,    54,    55,    56,    57,    58,    59,    60,    61,
      62,    17,    -1,    -1,    -1,    67,    68,    69,    70,    71,
      72,    73,    74,    75,    76,    31,    -1,    79,    80,    -1,
      -1,    -1,    38,    -1,    40,    -1,    42,    43,    44,    45,
      46,    47,    48,    49,    50,    51,    52,    53,    54,    55,
      56,    57,    58,    59,    60,    61,    62,    17,    -1,    -1,
      -1,    67,    68,    69,    70,    71,    72,    73,    74,    75,
      76,    31,    -1,    79,    80,    -1,    -1,    -1,    38,    -1,
      40,    -1,    42,    43,    44,    45,    46,    47,    48,    49,
      50,    51,    52,    53,    54,    55,    56,    57,    58,    59,
      60,    61,    62,    17,    -1,    -1,    -1,    67,    68,    69,
      70,    71,    72,    73,    74,    75,    76,    -1,    -1,    79,
      80,    -1,    36,    -1,    38,    -1,    -1,    -1,    42,    43,
      44,    45,    46,    47,    48,    49,    50,    51,    52,    53,
      54,    55,    56,    57,    58,    59,    60,    61,    62,    17,
      -1,    -1,    -1,    67,    68,    69,    70,    71,    72,    73,
      74,    75,    76,    -1,    -1,    79,    80,    -1,    -1,    -1,
      38,    -1,    40,    -1,    42,    43,    44,    45,    46,    47,
      48,    49,    50,    51,    52,    53,    54,    55,    56,    57,
      58,    59,    60,    61,    62,    -1,    17,    -1,    -1,    67,
      68,    69,    70,    71,    72,    73,    74,    75,    76,    -1,
      -1,    79,    80,    34,    -1,    -1,    -1,    38,    -1,    -1,
      -1,    42,    43,    44,    45,    46,    47,    48,    49,    50,
      51,    52,    53,    54,    55,    56,    57,    58,    59,    60,
      61,    62,    17,    -1,    -1,    -1,    67,    68,    69,    70,
      71,    72,    73,    74,    75,    76,    -1,    -1,    79,    80,
      -1,    -1,    -1,    38,    -1,    -1,    -1,    42,    43,    44,
      45,    46,    47,    48,    49,    50,    51,    52,    53,    54,
      55,    56,    57,    58,    59,    60,    61,    62,    17,    -1,
      -1,    -1,    67,    68,    69,    70,    71,    72,    73,    74,
      75,    76,    -1,    -1,    79,    80,    -1,    -1,    -1,    38,
      -1,    -1,    -1,    42,    43,    44,    45,    46,    47,    48,
      49,    50,    51,    52,    53,    54,    55,    56,    57,    58,
      59,    17,    -1,    62,    -1,    -1,    -1,    -1,    67,    68,
      69,    70,    71,    72,    73,    74,    75,    76,    -1,    -1,
      79,    80,    38,    -1,    -1,    -1,    42,    43,    44,    45,
      46,    47,    48,    49,    50,    51,    52,    53,    54,    55,
      56,    57,    58,    59,    17,    -1,    -1,    -1,    -1,    -1,
      -1,    67,    68,    69,    70,    71,    72,    73,    74,    75,
      76,    -1,    -1,    79,    80,    38,    -1,    -1,    -1,    42,
      43,    44,    45,    46,    47,    48,    49,    50,    51,    52,
      53,    54,    55,    56,    57,    58,    59,    17,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    68,    69,    70,    71,    72,
      73,    74,    75,    76,    -1,    -1,    79,    80,    38,    -1,
      -1,    -1,    -1,    -1,    44,    45,    46,    47,    48,    49,
      50,    51,    52,    53,    54,    55,    56,    57,    58,    59,
      17,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    68,    69,
      70,    71,    72,    73,    74,    75,    76,    -1,    -1,    79,
      80,    38,    -1,    -1,    -1,    -1,    -1,    -1,    45,    46,
      47,    48,    49,    50,    51,    52,    53,    54,    55,    56,
      57,    58,    59,    17,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    68,    69,    70,    71,    72,    73,    74,    75,    76,
      -1,    -1,    79,    80,    38,    -1,    -1,    -1,    -1,    -1,
      -1,    45,    46,    47,    48,    49,    50,    51,    52,    53,
      54,    -1,    56,    57,    58,    59,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    68,    69,    70,    71,    72,    73,
      74,    75,    76,    38,    -1,    79,    80,    -1,    -1,    -1,
      45,    46,    47,    48,    49,    50,    51,    52,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    68,    69,    70,    71,    72,    73,    74,
      75,    76,    -1,    -1,    79,    80
};

  /* YYSTOS[STATE-NUM] -- The (internal number of the) accessing
     symbol of state STATE-NUM.  */
static const yytype_int8 yystos[] =
{
       0,     1,     3,    94,    81,     4,     5,     6,     7,     8,
       9,    10,    11,    12,    13,    14,    15,    16,    19,    20,
      21,    22,    23,    24,    25,    26,    27,    28,    29,    30,
      31,    32,    63,    64,    69,    70,    71,    81,    82,    84,
      86,    88,    89,    91,    92,    95,    96,    97,     0,    81,
      81,    81,    81,    81,    81,    81,    81,    81,    82,    97,
     100,    97,    98,    95,    71,    97,   101,    95,    95,    97,
      97,    97,    95,    95,    95,    95,    95,    95,    97,    95,
     102,    95,    95,   102,   103,    86,    89,    97,   100,   102,
      97,    17,    38,    40,    42,    43,    44,    45,    46,    47,
      48,    49,    50,    51,    52,    53,    54,    55,    56,    57,
      58,    59,    60,    61,    62,    67,    68,    69,    70,    71,
      72,    73,    74,    75,    76,    79,    80,    81,    82,    82,
      83,   101,   100,    42,    43,    99,    40,    40,    40,    85,
      31,    31,    37,    99,    37,    99,    37,    99,    26,    27,
      33,    36,    39,    40,    83,    85,    84,    85,    41,    87,
     103,   102,    90,    82,    68,    85,    95,    81,    95,    95,
      95,    95,    95,    95,    95,    95,    95,    95,    95,    95,
      95,    95,    95,    95,    95,    95,    95,    95,    95,    95,
      95,    95,    95,    95,    95,    95,    95,    95,    83,   102,
      83,   102,    86,    99,    18,    83,    95,    95,    95,    97,
      95,    95,    95,    95,    95,    95,    95,    95,    95,    95,
      95,    95,    83,    41,    95,    95,   102,    87,    90,    83,
     102,    83,    85,    95,    83,    85,    83,    83,    87,   101,
      95,    83,    86,    99,    31,    35,    31,    35,    31,    35,
      34,    95,    83,    83,    95,    83,    68,    83,    95,    99,
      87,    86,    99,    87,   101,    95,    95,    95,    95,    95,
      95,    95,    95,    83,    83,    83,    95,    99,    87,   101,
      95,    99,    87,    31,    36,    31,    36,    31,    36,    95,
      99,    87,    95,    99,    95,    95,    95,    95,    95,    95,
      95,    99,    95,    31,    31,    31,    95,    95,    95,    95
};

  /* YYR1[YYN] -- Symbol number of symbol that rule YYN derives.  */
static const yytype_int8 yyr1[] =
{
       0,    93,    94,    94,    94,    94,    94,    94,    94,    94,
      94,    94,    94,    94,    94,    94,    95,    95,    95,    95,
      95,    95,    95,    95,    95,    95,    95,    95,    95,    95,
      95,    95,    95,    95,    95,    95,    95,    95,    95,    95,
      95,    95,    95,    95,    95,    95,    95,    95,    95,    95,
      95,    95,    95,    95,    95,    95,    95,    95,    95,    95,
      95,    95,    95,    95,    95,    95,    95,    95,    95,    95,
      95,    95,    95,    95,    95,    95,    95,    95,    95,    95,
      95,    95,    95,    95,    95,    95,    95,    95,    95,    95,
      95,    95,    95,    95,    95,    95,    95,    95,    95,    95,
      95,    95,    95,    95,    95,    95,    95,    95,    95,    95,
      96,    97,    98,    99,    99,   100,   100,   100,   100,   100,
     100,   100,   100,   100,   101,   101,   102,   102,   102,   103,
     103
};

  /* YYR2[YYN] -- Number of symbols on the right hand side of rule YYN.  */
static const yytype_int8 yyr2[] =
{
       0,     2,     3,     3,     3,     3,     3,     3,     3,     3,
       3,     3,     2,     4,     2,     1,     3,     4,     4,     3,
       4,     3,     3,     3,     3,     2,     4,     3,     3,     3,
       3,     3,     3,     3,     3,     3,     3,     3,     3,     3,
       3,     3,     3,     3,     3,     3,     3,     3,     3,     3,
       3,     2,     2,     2,     2,     2,     2,     2,     3,     3,
       3,     4,     4,     6,     5,     6,     5,     6,     3,     4,
       5,     3,     4,     4,     4,     4,     4,     8,    10,     6,
       8,    10,     6,     8,    10,     6,     4,     6,     1,     2,
       2,     1,     3,     4,     3,     4,     5,     4,     3,     2,
       2,     2,     2,     1,     1,     1,     1,     1,     1,     1,
       2,     1,     3,     1,     1,     5,     6,     4,     8,     9,
       7,     7,     8,     6,     3,     1,     3,     4,     1,     3,
       1
};


enum { YYENOMEM = -2 };

#define yyerrok         (yyerrstatus = 0)
#define yyclearin       (yychar = YYEMPTY)

#define YYACCEPT        goto yyacceptlab
#define YYABORT         goto yyabortlab
#define YYERROR         goto yyerrorlab


#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)                                    \
  do                                                              \
    if (yychar == YYEMPTY)                                        \
      {                                                           \
        yychar = (Token);                                         \
        yylval = (Value);                                         \
        YYPOPSTACK (yylen);                                       \
        yystate = *yyssp;                                         \
        goto yybackup;                                            \
      }                                                           \
    else                                                          \
      {                                                           \
        yyerror (YY_("syntax error: cannot back up")); \
        YYERROR;                                                  \
      }                                                           \
  while (0)

/* Backward compatibility with an undocumented macro.
   Use YYerror or YYUNDEF. */
#define YYERRCODE YYUNDEF


/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)                        \
do {                                            \
  if (yydebug)                                  \
    YYFPRINTF Args;                             \
} while (0)

/* This macro is provided for backward compatibility. */
# ifndef YY_LOCATION_PRINT
#  define YY_LOCATION_PRINT(File, Loc) ((void) 0)
# endif


# define YY_SYMBOL_PRINT(Title, Kind, Value, Location)                    \
do {                                                                      \
  if (yydebug)                                                            \
    {                                                                     \
      YYFPRINTF (stderr, "%s ", Title);                                   \
      yy_symbol_print (stderr,                                            \
                  Kind, Value); \
      YYFPRINTF (stderr, "\n");                                           \
    }                                                                     \
} while (0)


/*-----------------------------------.
| Print this symbol's value on YYO.  |
`-----------------------------------*/

static void
yy_symbol_value_print (FILE *yyo,
                       yysymbol_kind_t yykind, YYSTYPE const * const yyvaluep)
{
  FILE *yyoutput = yyo;
  YYUSE (yyoutput);
  if (!yyvaluep)
    return;
# ifdef YYPRINT
  if (yykind < YYNTOKENS)
    YYPRINT (yyo, yytoknum[yykind], *yyvaluep);
# endif
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  YYUSE (yykind);
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}


/*---------------------------.
| Print this symbol on YYO.  |
`---------------------------*/

static void
yy_symbol_print (FILE *yyo,
                 yysymbol_kind_t yykind, YYSTYPE const * const yyvaluep)
{
  YYFPRINTF (yyo, "%s %s (",
             yykind < YYNTOKENS ? "token" : "nterm", yysymbol_name (yykind));

  yy_symbol_value_print (yyo, yykind, yyvaluep);
  YYFPRINTF (yyo, ")");
}

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

static void
yy_stack_print (yy_state_t *yybottom, yy_state_t *yytop)
{
  YYFPRINTF (stderr, "Stack now");
  for (; yybottom <= yytop; yybottom++)
    {
      int yybot = *yybottom;
      YYFPRINTF (stderr, " %d", yybot);
    }
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)                            \
do {                                                            \
  if (yydebug)                                                  \
    yy_stack_print ((Bottom), (Top));                           \
} while (0)


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

static void
yy_reduce_print (yy_state_t *yyssp, YYSTYPE *yyvsp,
                 int yyrule)
{
  int yylno = yyrline[yyrule];
  int yynrhs = yyr2[yyrule];
  int yyi;
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %d):\n",
             yyrule - 1, yylno);
  /* The symbols being reduced.  */
  for (yyi = 0; yyi < yynrhs; yyi++)
    {
      YYFPRINTF (stderr, "   $%d = ", yyi + 1);
      yy_symbol_print (stderr,
                       YY_ACCESSING_SYMBOL (+yyssp[yyi + 1 - yynrhs]),
                       &yyvsp[(yyi + 1) - (yynrhs)]);
      YYFPRINTF (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)          \
do {                                    \
  if (yydebug)                          \
    yy_reduce_print (yyssp, yyvsp, Rule); \
} while (0)

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args) ((void) 0)
# define YY_SYMBOL_PRINT(Title, Kind, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif






/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

static void
yydestruct (const char *yymsg,
            yysymbol_kind_t yykind, YYSTYPE *yyvaluep)
{
  YYUSE (yyvaluep);
  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yykind, yyvaluep, yylocationp);

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  YYUSE (yykind);
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}


/* Lookahead token kind.  */
int yychar;

/* The semantic value of the lookahead symbol.  */
YYSTYPE yylval;
/* Number of syntax errors so far.  */
int yynerrs;




/*----------.
| yyparse.  |
`----------*/

int
yyparse (void)
{
    yy_state_fast_t yystate = 0;
    /* Number of tokens to shift before error messages enabled.  */
    int yyerrstatus = 0;

    /* Refer to the stacks through separate pointers, to allow yyoverflow
       to reallocate them elsewhere.  */

    /* Their size.  */
    YYPTRDIFF_T yystacksize = YYINITDEPTH;

    /* The state stack: array, bottom, top.  */
    yy_state_t yyssa[YYINITDEPTH];
    yy_state_t *yyss = yyssa;
    yy_state_t *yyssp = yyss;

    /* The semantic value stack: array, bottom, top.  */
    YYSTYPE yyvsa[YYINITDEPTH];
    YYSTYPE *yyvs = yyvsa;
    YYSTYPE *yyvsp = yyvs;

  int yyn;
  /* The return value of yyparse.  */
  int yyresult;
  /* Lookahead symbol kind.  */
  yysymbol_kind_t yytoken = YYSYMBOL_YYEMPTY;
  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;



#define YYPOPSTACK(N)   (yyvsp -= (N), yyssp -= (N))

  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int yylen = 0;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yychar = YYEMPTY; /* Cause a token to be read.  */
  goto yysetstate;


/*------------------------------------------------------------.
| yynewstate -- push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  yyssp++;


/*--------------------------------------------------------------------.
| yysetstate -- set current state (the top of the stack) to yystate.  |
`--------------------------------------------------------------------*/
yysetstate:
  YYDPRINTF ((stderr, "Entering state %d\n", yystate));
  YY_ASSERT (0 <= yystate && yystate < YYNSTATES);
  YY_IGNORE_USELESS_CAST_BEGIN
  *yyssp = YY_CAST (yy_state_t, yystate);
  YY_IGNORE_USELESS_CAST_END
  YY_STACK_PRINT (yyss, yyssp);

  if (yyss + yystacksize - 1 <= yyssp)
#if !defined yyoverflow && !defined YYSTACK_RELOCATE
    goto yyexhaustedlab;
#else
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYPTRDIFF_T yysize = yyssp - yyss + 1;

# if defined yyoverflow
      {
        /* Give user a chance to reallocate the stack.  Use copies of
           these so that the &'s don't force the real ones into
           memory.  */
        yy_state_t *yyss1 = yyss;
        YYSTYPE *yyvs1 = yyvs;

        /* Each stack pointer address is followed by the size of the
           data in use in that stack, in bytes.  This used to be a
           conditional around just the two extra args, but that might
           be undefined if yyoverflow is a macro.  */
        yyoverflow (YY_("memory exhausted"),
                    &yyss1, yysize * YYSIZEOF (*yyssp),
                    &yyvs1, yysize * YYSIZEOF (*yyvsp),
                    &yystacksize);
        yyss = yyss1;
        yyvs = yyvs1;
      }
# else /* defined YYSTACK_RELOCATE */
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
        goto yyexhaustedlab;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
        yystacksize = YYMAXDEPTH;

      {
        yy_state_t *yyss1 = yyss;
        union yyalloc *yyptr =
          YY_CAST (union yyalloc *,
                   YYSTACK_ALLOC (YY_CAST (YYSIZE_T, YYSTACK_BYTES (yystacksize))));
        if (! yyptr)
          goto yyexhaustedlab;
        YYSTACK_RELOCATE (yyss_alloc, yyss);
        YYSTACK_RELOCATE (yyvs_alloc, yyvs);
#  undef YYSTACK_RELOCATE
        if (yyss1 != yyssa)
          YYSTACK_FREE (yyss1);
      }
# endif

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;

      YY_IGNORE_USELESS_CAST_BEGIN
      YYDPRINTF ((stderr, "Stack size increased to %ld\n",
                  YY_CAST (long, yystacksize)));
      YY_IGNORE_USELESS_CAST_END

      if (yyss + yystacksize - 1 <= yyssp)
        YYABORT;
    }
#endif /* !defined yyoverflow && !defined YYSTACK_RELOCATE */

  if (yystate == YYFINAL)
    YYACCEPT;

  goto yybackup;


/*-----------.
| yybackup.  |
`-----------*/
yybackup:
  /* Do appropriate processing given the current state.  Read a
     lookahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to lookahead token.  */
  yyn = yypact[yystate];
  if (yypact_value_is_default (yyn))
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* YYCHAR is either empty, or end-of-input, or a valid lookahead.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token\n"));
      yychar = yylex ();
    }

  if (yychar <= YYEOF)
    {
      yychar = YYEOF;
      yytoken = YYSYMBOL_YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else if (yychar == YYerror)
    {
      /* The scanner already issued an error message, process directly
         to error recovery.  But do not keep the error token as
         lookahead, it is too special and may lead us to an endless
         loop in error recovery. */
      yychar = YYUNDEF;
      yytoken = YYSYMBOL_YYerror;
      goto yyerrlab1;
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yytable_value_is_error (yyn))
        goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  /* Shift the lookahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);
  yystate = yyn;
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END

  /* Discard the shifted token.  */
  yychar = YYEMPTY;
  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     '$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];


  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
  case 2: /* fullexpr: STARTTOK expr '\n'  */
#line 121 "parse.y"
                                   { YYACCEPT; }
#line 2018 "y.tab.c"
    break;

  case 3: /* fullexpr: STARTTOK LOADFILE '\n'  */
#line 122 "parse.y"
                                       { gel_command = GEL_LOADFILE; gel_command_arg = (yyvsp[-1].id); YYACCEPT; }
#line 2024 "y.tab.c"
    break;

  case 4: /* fullexpr: STARTTOK LOADFILE_GLOB '\n'  */
#line 123 "parse.y"
                                            { gel_command = GEL_LOADFILE_GLOB; gel_command_arg = (yyvsp[-1].id); YYACCEPT; }
#line 2030 "y.tab.c"
    break;

  case 5: /* fullexpr: STARTTOK CHANGEDIR '\n'  */
#line 124 "parse.y"
                                        { gel_command = GEL_CHANGEDIR; gel_command_arg = (yyvsp[-1].id); YYACCEPT; }
#line 2036 "y.tab.c"
    break;

  case 6: /* fullexpr: STARTTOK LS '\n'  */
#line 125 "parse.y"
                                 { gel_command = GEL_LS; YYACCEPT; }
#line 2042 "y.tab.c"
    break;

  case 7: /* fullexpr: STARTTOK LS_ARG '\n'  */
#line 126 "parse.y"
                                     { gel_command = GEL_LS_ARG; gel_command_arg = (yyvsp[-1].id); YYACCEPT; }
#line 2048 "y.tab.c"
    break;

  case 8: /* fullexpr: STARTTOK HELP '\n'  */
#line 127 "parse.y"
                                   { gel_command = GEL_HELP; YYACCEPT; }
#line 2054 "y.tab.c"
    break;

  case 9: /* fullexpr: STARTTOK HELP_ARG '\n'  */
#line 128 "parse.y"
                                       { gel_command = GEL_HELP_ARG; gel_command_arg = (yyvsp[-1].id); YYACCEPT; }
#line 2060 "y.tab.c"
    break;

  case 10: /* fullexpr: STARTTOK PWD '\n'  */
#line 129 "parse.y"
                                  { gel_command = GEL_PWD; YYACCEPT; }
#line 2066 "y.tab.c"
    break;

  case 11: /* fullexpr: STARTTOK LOAD_PLUGIN '\n'  */
#line 130 "parse.y"
                                          { gel_command = GEL_LOADPLUGIN; gel_command_arg = (yyvsp[-1].id); YYACCEPT; }
#line 2072 "y.tab.c"
    break;

  case 12: /* fullexpr: STARTTOK '\n'  */
#line 131 "parse.y"
                              { YYACCEPT; }
#line 2078 "y.tab.c"
    break;

  case 13: /* fullexpr: STARTTOK expr SEPAR '\n'  */
#line 132 "parse.y"
                                         { gp_push_null(); PUSH_ACT(GEL_E_SEPAR); YYACCEPT; }
#line 2084 "y.tab.c"
    break;

  case 14: /* fullexpr: error '\n'  */
#line 133 "parse.y"
                           { gel_return_ret = TRUE; yyclearin; YYABORT; }
#line 2090 "y.tab.c"
    break;

  case 15: /* fullexpr: error  */
#line 134 "parse.y"
                      { gel_return_ret = TRUE; }
#line 2096 "y.tab.c"
    break;

  case 16: /* expr: expr SEPAR expr  */
#line 137 "parse.y"
                                        { PUSH_ACT(GEL_E_SEPAR); }
#line 2102 "y.tab.c"
    break;

  case 17: /* expr: LOCAL '*' SEPAR expr  */
#line 138 "parse.y"
                                        { if ( ! gp_push_local_all ()) {
						SYNTAX_ERROR;
					  }
       					}
#line 2111 "y.tab.c"
    break;

  case 18: /* expr: LOCAL identlist SEPAR expr  */
#line 142 "parse.y"
                                           { if ( ! gp_push_local_idents ()) {
						SYNTAX_ERROR;
					  }
					}
#line 2120 "y.tab.c"
    break;

  case 19: /* expr: expr MOD expr  */
#line 146 "parse.y"
                                        { PUSH_ACT(GEL_E_MOD_CALC); }
#line 2126 "y.tab.c"
    break;

  case 20: /* expr: '(' expr SEPAR ')'  */
#line 147 "parse.y"
                                        { gp_push_null(); PUSH_ACT(GEL_E_SEPAR);
					  gp_push_spacer(); }
#line 2133 "y.tab.c"
    break;

  case 21: /* expr: '(' expr ')'  */
#line 149 "parse.y"
                                        { gp_push_spacer(); }
#line 2139 "y.tab.c"
    break;

  case 22: /* expr: '(' expr MAKEIMAGPARENTH  */
#line 150 "parse.y"
                                         { mpw_t i;
					  mpw_init (i);
					  mpw_i (i);
					  gp_push_spacer();
					  gel_stack_push(&gel_parsestack,
							 gel_makenum_use(i));
					  PUSH_ACT(GEL_E_MUL); }
#line 2151 "y.tab.c"
    break;

  case 23: /* expr: expr EQUALS expr  */
#line 157 "parse.y"
                                        { PUSH_ACT(GEL_E_EQUALS); }
#line 2157 "y.tab.c"
    break;

  case 24: /* expr: expr DEFEQUALS expr  */
#line 158 "parse.y"
                                        { PUSH_ACT(GEL_E_DEFEQUALS); }
#line 2163 "y.tab.c"
    break;

  case 25: /* expr: INCREMENT expr  */
#line 159 "parse.y"
                                                          { PUSH_ACT(GEL_E_INCREMENT); }
#line 2169 "y.tab.c"
    break;

  case 26: /* expr: INCREMENT expr BY expr  */
#line 160 "parse.y"
                                                       { PUSH_ACT(GEL_E_INCREMENT_BY); }
#line 2175 "y.tab.c"
    break;

  case 27: /* expr: expr SWAPWITH expr  */
#line 161 "parse.y"
                                        { PUSH_ACT(GEL_E_SWAPWITH); }
#line 2181 "y.tab.c"
    break;

  case 28: /* expr: '|' expr '|'  */
#line 162 "parse.y"
                                        { PUSH_ACT(GEL_E_ABS); }
#line 2187 "y.tab.c"
    break;

  case 29: /* expr: expr '+' expr  */
#line 163 "parse.y"
                                        { PUSH_ACT(GEL_E_PLUS); }
#line 2193 "y.tab.c"
    break;

  case 30: /* expr: expr ELTELTPLUS expr  */
#line 164 "parse.y"
                                        { PUSH_ACT(GEL_E_ELTPLUS); }
#line 2199 "y.tab.c"
    break;

  case 31: /* expr: expr '-' expr  */
#line 165 "parse.y"
                                        { PUSH_ACT(GEL_E_MINUS); }
#line 2205 "y.tab.c"
    break;

  case 32: /* expr: expr ELTELTMINUS expr  */
#line 166 "parse.y"
                                        { PUSH_ACT(GEL_E_ELTMINUS); }
#line 2211 "y.tab.c"
    break;

  case 33: /* expr: expr '*' expr  */
#line 167 "parse.y"
                                        { PUSH_ACT(GEL_E_MUL); }
#line 2217 "y.tab.c"
    break;

  case 34: /* expr: expr ELTELTMUL expr  */
#line 168 "parse.y"
                                        { PUSH_ACT(GEL_E_ELTMUL); }
#line 2223 "y.tab.c"
    break;

  case 35: /* expr: expr '/' expr  */
#line 169 "parse.y"
                                        { PUSH_ACT(GEL_E_DIV); }
#line 2229 "y.tab.c"
    break;

  case 36: /* expr: expr ELTELTDIV expr  */
#line 170 "parse.y"
                                        { PUSH_ACT(GEL_E_ELTDIV); }
#line 2235 "y.tab.c"
    break;

  case 37: /* expr: expr '\\' expr  */
#line 171 "parse.y"
                                        { PUSH_ACT(GEL_E_BACK_DIV); }
#line 2241 "y.tab.c"
    break;

  case 38: /* expr: expr ELTELTBACKDIV expr  */
#line 172 "parse.y"
                                        { PUSH_ACT(GEL_E_ELT_BACK_DIV); }
#line 2247 "y.tab.c"
    break;

  case 39: /* expr: expr '%' expr  */
#line 173 "parse.y"
                                        { PUSH_ACT(GEL_E_MOD); }
#line 2253 "y.tab.c"
    break;

  case 40: /* expr: expr ELTELTMOD expr  */
#line 174 "parse.y"
                                        { PUSH_ACT(GEL_E_ELTMOD); }
#line 2259 "y.tab.c"
    break;

  case 41: /* expr: expr CMP_CMP expr  */
#line 175 "parse.y"
                                        { PUSH_ACT(GEL_E_CMP_CMP); }
#line 2265 "y.tab.c"
    break;

  case 42: /* expr: expr EQ_CMP expr  */
#line 177 "parse.y"
                                        { PUSH_ACT(GEL_E_EQ_CMP); }
#line 2271 "y.tab.c"
    break;

  case 43: /* expr: expr NE_CMP expr  */
#line 178 "parse.y"
                                        { PUSH_ACT(GEL_E_NE_CMP); }
#line 2277 "y.tab.c"
    break;

  case 44: /* expr: expr LT_CMP expr  */
#line 179 "parse.y"
                                        { PUSH_ACT(GEL_E_LT_CMP); }
#line 2283 "y.tab.c"
    break;

  case 45: /* expr: expr GT_CMP expr  */
#line 180 "parse.y"
                                        { PUSH_ACT(GEL_E_GT_CMP); }
#line 2289 "y.tab.c"
    break;

  case 46: /* expr: expr LE_CMP expr  */
#line 181 "parse.y"
                                        { PUSH_ACT(GEL_E_LE_CMP); }
#line 2295 "y.tab.c"
    break;

  case 47: /* expr: expr GE_CMP expr  */
#line 182 "parse.y"
                                        { PUSH_ACT(GEL_E_GE_CMP); }
#line 2301 "y.tab.c"
    break;

  case 48: /* expr: expr LOGICAL_AND expr  */
#line 184 "parse.y"
                                        { PUSH_ACT(GEL_E_LOGICAL_AND); }
#line 2307 "y.tab.c"
    break;

  case 49: /* expr: expr LOGICAL_OR expr  */
#line 185 "parse.y"
                                        { PUSH_ACT(GEL_E_LOGICAL_OR); }
#line 2313 "y.tab.c"
    break;

  case 50: /* expr: expr LOGICAL_XOR expr  */
#line 186 "parse.y"
                                        { PUSH_ACT(GEL_E_LOGICAL_XOR); }
#line 2319 "y.tab.c"
    break;

  case 51: /* expr: LOGICAL_NOT expr  */
#line 187 "parse.y"
                                        { PUSH_ACT(GEL_E_LOGICAL_NOT); }
#line 2325 "y.tab.c"
    break;

  case 52: /* expr: expr '!'  */
#line 192 "parse.y"
                                        { PUSH_ACT(GEL_E_FACT); }
#line 2331 "y.tab.c"
    break;

  case 53: /* expr: expr DOUBLEFACT  */
#line 193 "parse.y"
                                        { PUSH_ACT(GEL_E_DBLFACT); }
#line 2337 "y.tab.c"
    break;

  case 54: /* expr: expr '\''  */
#line 194 "parse.y"
                                        { PUSH_ACT(GEL_E_CONJUGATE_TRANSPOSE); }
#line 2343 "y.tab.c"
    break;

  case 55: /* expr: expr TRANSPOSE  */
#line 195 "parse.y"
                                        { PUSH_ACT(GEL_E_TRANSPOSE); }
#line 2349 "y.tab.c"
    break;

  case 56: /* expr: '-' expr  */
#line 196 "parse.y"
                                        { PUSH_ACT(GEL_E_NEG); }
#line 2355 "y.tab.c"
    break;

  case 58: /* expr: expr '^' expr  */
#line 198 "parse.y"
                                        { PUSH_ACT(GEL_E_EXP); }
#line 2361 "y.tab.c"
    break;

  case 59: /* expr: expr ELTELTEXP expr  */
#line 199 "parse.y"
                                        { PUSH_ACT(GEL_E_ELTEXP); }
#line 2367 "y.tab.c"
    break;

  case 60: /* expr: expr ':' expr  */
#line 201 "parse.y"
                                {
				if (gp_prepare_push_region_sep ()) {
					PUSH_ACT(GEL_E_REGION_SEP_BY);
				} else {
					PUSH_ACT(GEL_E_REGION_SEP);
				}
					}
#line 2379 "y.tab.c"
    break;

  case 61: /* expr: expr AT ':' ')'  */
#line 209 "parse.y"
                                        { /* FIXME: do nothing?, this is just a 
					     get all */ }
#line 2386 "y.tab.c"
    break;

  case 62: /* expr: expr AT expr ')'  */
#line 211 "parse.y"
                                        { PUSH_ACT(GEL_E_GET_VELEMENT); }
#line 2392 "y.tab.c"
    break;

  case 63: /* expr: expr AT expr ',' expr ')'  */
#line 212 "parse.y"
                                          { PUSH_ACT(GEL_E_GET_ELEMENT); }
#line 2398 "y.tab.c"
    break;

  case 64: /* expr: expr AT expr ',' ')'  */
#line 213 "parse.y"
                                        { PUSH_ACT(GEL_E_GET_ROW_REGION); }
#line 2404 "y.tab.c"
    break;

  case 65: /* expr: expr AT expr ',' ':' ')'  */
#line 214 "parse.y"
                                                { PUSH_ACT(GEL_E_GET_ROW_REGION); }
#line 2410 "y.tab.c"
    break;

  case 66: /* expr: expr AT ',' expr ')'  */
#line 215 "parse.y"
                                        { PUSH_ACT(GEL_E_GET_COL_REGION); }
#line 2416 "y.tab.c"
    break;

  case 67: /* expr: expr AT ':' ',' expr ')'  */
#line 216 "parse.y"
                                                { PUSH_ACT(GEL_E_GET_COL_REGION); }
#line 2422 "y.tab.c"
    break;

  case 68: /* expr: '[' matrixrows ']'  */
#line 217 "parse.y"
                                        { if(!gp_push_matrix(FALSE)) {SYNTAX_ERROR;} }
#line 2428 "y.tab.c"
    break;

  case 69: /* expr: '`' '[' matrixrows ']'  */
#line 218 "parse.y"
                                        { if(!gp_push_matrix(TRUE)) {SYNTAX_ERROR;} }
#line 2434 "y.tab.c"
    break;

  case 70: /* expr: '(' exprlist ',' expr ')'  */
#line 225 "parse.y"
                                                {
			if(!gp_push_matrix_row()) {SYNTAX_ERROR;}
			if(!gp_push_marker(GEL_MATRIX_START_NODE)) {SYNTAX_ERROR;}
			if(!gp_push_matrix(TRUE)) {SYNTAX_ERROR;}
					}
#line 2444 "y.tab.c"
    break;

  case 71: /* expr: '{' exprlist '}'  */
#line 230 "parse.y"
                                        {SYNTAX_ERROR;}
#line 2450 "y.tab.c"
    break;

  case 72: /* expr: '`' '{' exprlist '}'  */
#line 231 "parse.y"
                                        {SYNTAX_ERROR;}
#line 2456 "y.tab.c"
    break;

  case 73: /* expr: WHILE expr DO expr  */
#line 232 "parse.y"
                                        { PUSH_ACT(GEL_E_WHILE_CONS); }
#line 2462 "y.tab.c"
    break;

  case 74: /* expr: UNTIL expr DO expr  */
#line 233 "parse.y"
                                        { PUSH_ACT(GEL_E_UNTIL_CONS); }
#line 2468 "y.tab.c"
    break;

  case 75: /* expr: DO expr WHILE expr  */
#line 234 "parse.y"
                                        { PUSH_ACT(GEL_E_DOWHILE_CONS); }
#line 2474 "y.tab.c"
    break;

  case 76: /* expr: DO expr UNTIL expr  */
#line 235 "parse.y"
                                        { PUSH_ACT(GEL_E_DOUNTIL_CONS); }
#line 2480 "y.tab.c"
    break;

  case 77: /* expr: FOR ident anyequals expr TO expr DO expr  */
#line 236 "parse.y"
                                                         { PUSH_ACT(GEL_E_FOR_CONS); }
#line 2486 "y.tab.c"
    break;

  case 78: /* expr: FOR ident anyequals expr TO expr BY expr DO expr  */
#line 237 "parse.y"
                                                                 { PUSH_ACT(GEL_E_FORBY_CONS); }
#line 2492 "y.tab.c"
    break;

  case 79: /* expr: FOR ident IN expr DO expr  */
#line 238 "parse.y"
                                          { PUSH_ACT(GEL_E_FORIN_CONS); }
#line 2498 "y.tab.c"
    break;

  case 80: /* expr: SUM ident anyequals expr TO expr DO expr  */
#line 239 "parse.y"
                                                         { PUSH_ACT(GEL_E_SUM_CONS); }
#line 2504 "y.tab.c"
    break;

  case 81: /* expr: SUM ident anyequals expr TO expr BY expr DO expr  */
#line 240 "parse.y"
                                                                 { PUSH_ACT(GEL_E_SUMBY_CONS); }
#line 2510 "y.tab.c"
    break;

  case 82: /* expr: SUM ident IN expr DO expr  */
#line 241 "parse.y"
                                          { PUSH_ACT(GEL_E_SUMIN_CONS); }
#line 2516 "y.tab.c"
    break;

  case 83: /* expr: PROD ident anyequals expr TO expr DO expr  */
#line 242 "parse.y"
                                                          { PUSH_ACT(GEL_E_PROD_CONS); }
#line 2522 "y.tab.c"
    break;

  case 84: /* expr: PROD ident anyequals expr TO expr BY expr DO expr  */
#line 243 "parse.y"
                                                                  { PUSH_ACT(GEL_E_PRODBY_CONS); }
#line 2528 "y.tab.c"
    break;

  case 85: /* expr: PROD ident IN expr DO expr  */
#line 244 "parse.y"
                                           { PUSH_ACT(GEL_E_PRODIN_CONS); }
#line 2534 "y.tab.c"
    break;

  case 86: /* expr: IF expr THEN expr  */
#line 245 "parse.y"
                                                        { PUSH_ACT(GEL_E_IF_CONS); }
#line 2540 "y.tab.c"
    break;

  case 87: /* expr: IF expr THEN expr ELSE expr  */
#line 246 "parse.y"
                                            { PUSH_ACT(GEL_E_IFELSE_CONS); }
#line 2546 "y.tab.c"
    break;

  case 88: /* expr: ident  */
#line 247 "parse.y"
                                        { gp_convert_identifier_to_bool ();
					  /* convert true/false to bool */}
#line 2553 "y.tab.c"
    break;

  case 89: /* expr: '`' ident  */
#line 249 "parse.y"
                                        { PUSH_ACT(GEL_E_QUOTE); }
#line 2559 "y.tab.c"
    break;

  case 90: /* expr: '&' ident  */
#line 250 "parse.y"
                                        { PUSH_ACT(GEL_E_REFERENCE); }
#line 2565 "y.tab.c"
    break;

  case 92: /* expr: ident '(' ')'  */
#line 252 "parse.y"
                                        { gp_push_marker_simple(GEL_EXPRLIST_START_NODE);
					  PUSH_ACT(GEL_E_DIRECTCALL); }
#line 2572 "y.tab.c"
    break;

  case 93: /* expr: ident '(' exprlist ')'  */
#line 254 "parse.y"
                                        { PUSH_ACT(GEL_E_DIRECTCALL); }
#line 2578 "y.tab.c"
    break;

  case 94: /* expr: deref '(' ')'  */
#line 255 "parse.y"
                                        { gp_push_marker_simple(GEL_EXPRLIST_START_NODE);
					  PUSH_ACT(GEL_E_DIRECTCALL); }
#line 2585 "y.tab.c"
    break;

  case 95: /* expr: deref '(' exprlist ')'  */
#line 257 "parse.y"
                                        { PUSH_ACT(GEL_E_DIRECTCALL); }
#line 2591 "y.tab.c"
    break;

  case 96: /* expr: expr CALL '(' exprlist ')'  */
#line 258 "parse.y"
                                           { PUSH_ACT(GEL_E_CALL); }
#line 2597 "y.tab.c"
    break;

  case 97: /* expr: expr CALL '(' ')'  */
#line 259 "parse.y"
                                        { gp_push_marker_simple(GEL_EXPRLIST_START_NODE);
					  PUSH_ACT(GEL_E_CALL); }
#line 2604 "y.tab.c"
    break;

  case 98: /* expr: FUNCTION ident funcdef  */
#line 261 "parse.y"
                                        { PUSH_ACT(GEL_E_DEFEQUALS); }
#line 2610 "y.tab.c"
    break;

  case 102: /* expr: RETURNTOK expr  */
#line 265 "parse.y"
                                        { PUSH_ACT(GEL_E_RETURN); }
#line 2616 "y.tab.c"
    break;

  case 103: /* expr: BAILOUT  */
#line 266 "parse.y"
                                        { PUSH_ACT(GEL_E_BAILOUT); }
#line 2622 "y.tab.c"
    break;

  case 104: /* expr: EXCEPTION  */
#line 267 "parse.y"
                                        { PUSH_ACT(GEL_E_EXCEPTION); }
#line 2628 "y.tab.c"
    break;

  case 105: /* expr: CONTINUE  */
#line 268 "parse.y"
                                        { PUSH_ACT(GEL_E_CONTINUE); }
#line 2634 "y.tab.c"
    break;

  case 106: /* expr: BREAK  */
#line 269 "parse.y"
                                        { PUSH_ACT(GEL_E_BREAK); }
#line 2640 "y.tab.c"
    break;

  case 107: /* expr: NUMBER  */
#line 270 "parse.y"
                                        { gel_stack_push(&gel_parsestack,
							 gel_makenum_use((yyvsp[0].val))); }
#line 2647 "y.tab.c"
    break;

  case 108: /* expr: STRING  */
#line 272 "parse.y"
                                        { PUSH_CONST_STRING((yyvsp[0].id)); }
#line 2653 "y.tab.c"
    break;

  case 109: /* expr: '.'  */
#line 273 "parse.y"
                                        { gp_push_null(); }
#line 2659 "y.tab.c"
    break;

  case 110: /* deref: '*' ident  */
#line 276 "parse.y"
                                        { PUSH_ACT(GEL_E_DEREFERENCE); }
#line 2665 "y.tab.c"
    break;

  case 111: /* ident: FUNCID  */
#line 279 "parse.y"
                                        { PUSH_IDENTIFIER((yyvsp[0].id)); }
#line 2671 "y.tab.c"
    break;

  case 112: /* paramdef: ident anyequals expr  */
#line 282 "parse.y"
                                                  {
			gp_prepare_push_param (FALSE);
			PUSH_ACT (GEL_E_PARAMETER);
		}
#line 2680 "y.tab.c"
    break;

  case 115: /* funcdef: '(' identlist ')' anyequals expr  */
#line 296 "parse.y"
                                                                {
			if ( ! gp_push_func (FALSE /* vararg */,
					     TRUE /* arguments */,
					     FALSE /* extradict */,
					     FALSE /* never_subst */)) {
				SYNTAX_ERROR;
			}
						}
#line 2693 "y.tab.c"
    break;

  case 116: /* funcdef: '(' identlist THREEDOTS ')' anyequals expr  */
#line 304 "parse.y"
                                                                          {
			if ( ! gp_push_func (TRUE /* vararg */,
					     TRUE /* arguments */,
					     FALSE /* extradict */,
					     FALSE /* never_subst */)) {
				SYNTAX_ERROR;
			}
							}
#line 2706 "y.tab.c"
    break;

  case 117: /* funcdef: '(' ')' anyequals expr  */
#line 312 "parse.y"
                                                      {
			if ( ! gp_push_func (FALSE /* vararg */,
					     FALSE /* arguments */,
					     FALSE /* extradict */,
					     FALSE /* never_subst */)) {
				SYNTAX_ERROR;
			}
					}
#line 2719 "y.tab.c"
    break;

  case 118: /* funcdef: '(' identlist ')' '[' identlist ']' anyequals expr  */
#line 320 "parse.y"
                                                                                  {
			if ( ! gp_push_func (FALSE /* vararg */,
					     TRUE /* arguments */,
					     TRUE /* extradict */,
					     TRUE /* never_subst */)) {
				SYNTAX_ERROR;
			}
						}
#line 2732 "y.tab.c"
    break;

  case 119: /* funcdef: '(' identlist THREEDOTS ')' '[' identlist ']' anyequals expr  */
#line 328 "parse.y"
                                                                                            {
			if ( ! gp_push_func (TRUE /* vararg */,
					     TRUE /* arguments */,
					     TRUE /* extradict */,
					     TRUE /* never_subst */)) {
				SYNTAX_ERROR;
			}
							}
#line 2745 "y.tab.c"
    break;

  case 120: /* funcdef: '(' ')' '[' identlist ']' anyequals expr  */
#line 336 "parse.y"
                                                                        {
			if ( ! gp_push_func (FALSE /* vararg */,
					     FALSE /* arguments */,
					     TRUE /* extradict */,
					     TRUE /* never_subst */)) {
				SYNTAX_ERROR;
			}
					}
#line 2758 "y.tab.c"
    break;

  case 121: /* funcdef: '(' identlist ')' '[' ']' anyequals expr  */
#line 345 "parse.y"
                                                                        {
			if ( ! gp_push_func (FALSE /* vararg */,
					     TRUE /* arguments */,
					     FALSE /* extradict */,
					     TRUE /* never_subst */)) {
				SYNTAX_ERROR;
			}
						}
#line 2771 "y.tab.c"
    break;

  case 122: /* funcdef: '(' identlist THREEDOTS ')' '[' ']' anyequals expr  */
#line 353 "parse.y"
                                                                                  {
			if ( ! gp_push_func (TRUE /* vararg */,
					     TRUE /* arguments */,
					     FALSE /* extradict */,
					     TRUE /* never_subst */)) {
				SYNTAX_ERROR;
			}
							}
#line 2784 "y.tab.c"
    break;

  case 123: /* funcdef: '(' ')' '[' ']' anyequals expr  */
#line 361 "parse.y"
                                                              {
			if ( ! gp_push_func (FALSE /* vararg */,
					     FALSE /* arguments */,
					     FALSE /* extradict */,
					     TRUE /* never_subst */)) {
				SYNTAX_ERROR;
			}
					}
#line 2797 "y.tab.c"
    break;

  case 125: /* identlist: ident  */
#line 372 "parse.y"
                      { if(!gp_push_marker(GEL_EXPRLIST_START_NODE)) {SYNTAX_ERROR;} }
#line 2803 "y.tab.c"
    break;

  case 128: /* exprlist: expr  */
#line 379 "parse.y"
                     { if(!gp_push_marker(GEL_EXPRLIST_START_NODE)) {SYNTAX_ERROR;} }
#line 2809 "y.tab.c"
    break;

  case 129: /* matrixrows: matrixrows NEXTROW exprlist  */
#line 382 "parse.y"
                                            { if(!gp_push_matrix_row()) {SYNTAX_ERROR;} }
#line 2815 "y.tab.c"
    break;

  case 130: /* matrixrows: exprlist  */
#line 383 "parse.y"
                         { if(!gp_push_matrix_row()) {SYNTAX_ERROR;} if(!gp_push_marker(GEL_MATRIX_START_NODE)) {SYNTAX_ERROR;} }
#line 2821 "y.tab.c"
    break;


#line 2825 "y.tab.c"

      default: break;
    }
  /* User semantic actions sometimes alter yychar, and that requires
     that yytoken be updated with the new translation.  We take the
     approach of translating immediately before every use of yytoken.
     One alternative is translating here after every semantic action,
     but that translation would be missed if the semantic action invokes
     YYABORT, YYACCEPT, or YYERROR immediately after altering yychar or
     if it invokes YYBACKUP.  In the case of YYABORT or YYACCEPT, an
     incorrect destructor might then be invoked immediately.  In the
     case of YYERROR or YYBACKUP, subsequent parser actions might lead
     to an incorrect destructor call or verbose syntax error message
     before the lookahead is translated.  */
  YY_SYMBOL_PRINT ("-> $$ =", YY_CAST (yysymbol_kind_t, yyr1[yyn]), &yyval, &yyloc);

  YYPOPSTACK (yylen);
  yylen = 0;

  *++yyvsp = yyval;

  /* Now 'shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */
  {
    const int yylhs = yyr1[yyn] - YYNTOKENS;
    const int yyi = yypgoto[yylhs] + *yyssp;
    yystate = (0 <= yyi && yyi <= YYLAST && yycheck[yyi] == *yyssp
               ? yytable[yyi]
               : yydefgoto[yylhs]);
  }

  goto yynewstate;


/*--------------------------------------.
| yyerrlab -- here on detecting error.  |
`--------------------------------------*/
yyerrlab:
  /* Make sure we have latest lookahead translation.  See comments at
     user semantic actions for why this is necessary.  */
  yytoken = yychar == YYEMPTY ? YYSYMBOL_YYEMPTY : YYTRANSLATE (yychar);
  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
      yyerror (YY_("syntax error"));
    }

  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse lookahead token after an
         error, discard it.  */

      if (yychar <= YYEOF)
        {
          /* Return failure if at end of input.  */
          if (yychar == YYEOF)
            YYABORT;
        }
      else
        {
          yydestruct ("Error: discarding",
                      yytoken, &yylval);
          yychar = YYEMPTY;
        }
    }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:
  /* Pacify compilers when the user code never invokes YYERROR and the
     label yyerrorlab therefore never appears in user code.  */
  if (0)
    YYERROR;

  /* Do not reclaim the symbols of the rule whose action triggered
     this YYERROR.  */
  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;      /* Each real token shifted decrements this.  */

  /* Pop stack until we find a state that shifts the error token.  */
  for (;;)
    {
      yyn = yypact[yystate];
      if (!yypact_value_is_default (yyn))
        {
          yyn += YYSYMBOL_YYerror;
          if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYSYMBOL_YYerror)
            {
              yyn = yytable[yyn];
              if (0 < yyn)
                break;
            }
        }

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
        YYABORT;


      yydestruct ("Error: popping",
                  YY_ACCESSING_SYMBOL (yystate), yyvsp);
      YYPOPSTACK (1);
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END


  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", YY_ACCESSING_SYMBOL (yyn), yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturn;


/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturn;


#if !defined yyoverflow
/*-------------------------------------------------.
| yyexhaustedlab -- memory exhaustion comes here.  |
`-------------------------------------------------*/
yyexhaustedlab:
  yyerror (YY_("memory exhausted"));
  yyresult = 2;
  goto yyreturn;
#endif


/*-------------------------------------------------------.
| yyreturn -- parsing is finished, clean up and return.  |
`-------------------------------------------------------*/
yyreturn:
  if (yychar != YYEMPTY)
    {
      /* Make sure we have latest lookahead translation.  See comments at
         user semantic actions for why this is necessary.  */
      yytoken = YYTRANSLATE (yychar);
      yydestruct ("Cleanup: discarding lookahead",
                  yytoken, &yylval);
    }
  /* Do not reclaim the symbols of the rule whose action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (yylen);
  YY_STACK_PRINT (yyss, yyssp);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
                  YY_ACCESSING_SYMBOL (+*yyssp), yyvsp);
      YYPOPSTACK (1);
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif

  return yyresult;
}

#line 386 "parse.y"

