/* A Bison parser, made by GNU Bison 3.7.  */

/* Bison interface for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015, 2018-2020 Free Software Foundation,
   Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* DO NOT RELY ON FEATURES THAT ARE NOT DOCUMENTED in the manual,
   especially those whose name start with YY_ or yy_.  They are
   private implementation details that can be changed or removed.  */

#ifndef YY_YY_Y_TAB_H_INCLUDED
# define YY_YY_Y_TAB_H_INCLUDED
/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 1
#endif
#if YYDEBUG
extern int yydebug;
#endif

/* Token kinds.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    YYEMPTY = -2,
    YYEOF = 0,                     /* "end of file"  */
    YYerror = 256,                 /* error  */
    YYUNDEF = 257,                 /* "invalid token"  */
    STARTTOK = 258,                /* STARTTOK  */
    LOADFILE = 259,                /* LOADFILE  */
    LOADFILE_GLOB = 260,           /* LOADFILE_GLOB  */
    LOAD_PLUGIN = 261,             /* LOAD_PLUGIN  */
    CHANGEDIR = 262,               /* CHANGEDIR  */
    PWD = 263,                     /* PWD  */
    LS = 264,                      /* LS  */
    LS_ARG = 265,                  /* LS_ARG  */
    HELP = 266,                    /* HELP  */
    HELP_ARG = 267,                /* HELP_ARG  */
    NUMBER = 268,                  /* NUMBER  */
    STRING = 269,                  /* STRING  */
    FUNCID = 270,                  /* FUNCID  */
    FUNCTION = 271,                /* FUNCTION  */
    CALL = 272,                    /* CALL  */
    THREEDOTS = 273,               /* THREEDOTS  */
    PARAMETER = 274,               /* PARAMETER  */
    RETURNTOK = 275,               /* RETURNTOK  */
    BAILOUT = 276,                 /* BAILOUT  */
    EXCEPTION = 277,               /* EXCEPTION  */
    CONTINUE = 278,                /* CONTINUE  */
    BREAK = 279,                   /* BREAK  */
    LOCAL = 280,                   /* LOCAL  */
    WHILE = 281,                   /* WHILE  */
    UNTIL = 282,                   /* UNTIL  */
    FOR = 283,                     /* FOR  */
    SUM = 284,                     /* SUM  */
    PROD = 285,                    /* PROD  */
    DO = 286,                      /* DO  */
    IF = 287,                      /* IF  */
    THEN = 288,                    /* THEN  */
    ELSE = 289,                    /* ELSE  */
    TO = 290,                      /* TO  */
    BY = 291,                      /* BY  */
    IN = 292,                      /* IN  */
    AT = 293,                      /* AT  */
    MAKEIMAGPARENTH = 294,         /* MAKEIMAGPARENTH  */
    SEPAR = 295,                   /* SEPAR  */
    NEXTROW = 296,                 /* NEXTROW  */
    EQUALS = 297,                  /* EQUALS  */
    DEFEQUALS = 298,               /* DEFEQUALS  */
    SWAPWITH = 299,                /* SWAPWITH  */
    TRANSPOSE = 300,               /* TRANSPOSE  */
    ELTELTDIV = 301,               /* ELTELTDIV  */
    ELTELTMUL = 302,               /* ELTELTMUL  */
    ELTELTPLUS = 303,              /* ELTELTPLUS  */
    ELTELTMINUS = 304,             /* ELTELTMINUS  */
    ELTELTEXP = 305,               /* ELTELTEXP  */
    ELTELTMOD = 306,               /* ELTELTMOD  */
    DOUBLEFACT = 307,              /* DOUBLEFACT  */
    EQ_CMP = 308,                  /* EQ_CMP  */
    NE_CMP = 309,                  /* NE_CMP  */
    CMP_CMP = 310,                 /* CMP_CMP  */
    LT_CMP = 311,                  /* LT_CMP  */
    GT_CMP = 312,                  /* GT_CMP  */
    LE_CMP = 313,                  /* LE_CMP  */
    GE_CMP = 314,                  /* GE_CMP  */
    LOGICAL_XOR = 315,             /* LOGICAL_XOR  */
    LOGICAL_OR = 316,              /* LOGICAL_OR  */
    LOGICAL_AND = 317,             /* LOGICAL_AND  */
    LOGICAL_NOT = 318,             /* LOGICAL_NOT  */
    INCREMENT = 319,               /* INCREMENT  */
    LOWER_THAN_ELSE = 320,         /* LOWER_THAN_ELSE  */
    LOWER_THAN_INCREMENT = 321,    /* LOWER_THAN_INCREMENT  */
    MOD = 322,                     /* MOD  */
    ELTELTBACKDIV = 323,           /* ELTELTBACKDIV  */
    UMINUS = 324,                  /* UMINUS  */
    UPLUS = 325                    /* UPLUS  */
  };
  typedef enum yytokentype yytoken_kind_t;
#endif
/* Token kinds.  */
#define YYEOF 0
#define YYerror 256
#define YYUNDEF 257
#define STARTTOK 258
#define LOADFILE 259
#define LOADFILE_GLOB 260
#define LOAD_PLUGIN 261
#define CHANGEDIR 262
#define PWD 263
#define LS 264
#define LS_ARG 265
#define HELP 266
#define HELP_ARG 267
#define NUMBER 268
#define STRING 269
#define FUNCID 270
#define FUNCTION 271
#define CALL 272
#define THREEDOTS 273
#define PARAMETER 274
#define RETURNTOK 275
#define BAILOUT 276
#define EXCEPTION 277
#define CONTINUE 278
#define BREAK 279
#define LOCAL 280
#define WHILE 281
#define UNTIL 282
#define FOR 283
#define SUM 284
#define PROD 285
#define DO 286
#define IF 287
#define THEN 288
#define ELSE 289
#define TO 290
#define BY 291
#define IN 292
#define AT 293
#define MAKEIMAGPARENTH 294
#define SEPAR 295
#define NEXTROW 296
#define EQUALS 297
#define DEFEQUALS 298
#define SWAPWITH 299
#define TRANSPOSE 300
#define ELTELTDIV 301
#define ELTELTMUL 302
#define ELTELTPLUS 303
#define ELTELTMINUS 304
#define ELTELTEXP 305
#define ELTELTMOD 306
#define DOUBLEFACT 307
#define EQ_CMP 308
#define NE_CMP 309
#define CMP_CMP 310
#define LT_CMP 311
#define GT_CMP 312
#define LE_CMP 313
#define GE_CMP 314
#define LOGICAL_XOR 315
#define LOGICAL_OR 316
#define LOGICAL_AND 317
#define LOGICAL_NOT 318
#define INCREMENT 319
#define LOWER_THAN_ELSE 320
#define LOWER_THAN_INCREMENT 321
#define MOD 322
#define ELTELTBACKDIV 323
#define UMINUS 324
#define UPLUS 325

/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
union YYSTYPE
{
#line 47 "parse.y"

	mpw_t val;
	char *id;

#line 211 "y.tab.h"

};
typedef union YYSTYPE YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif


extern YYSTYPE yylval;

int yyparse (void);

#endif /* !YY_YY_Y_TAB_H_INCLUDED  */
